QT += core
QT -= gui
QT += network
QT += serialport
#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
CONFIG += console

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    logsubsystem.cpp \
    coreserver.cpp \
    main.cpp \
    protocolhandler.cpp \
    serialserver.cpp \
    sslclient.cpp # \
#    widget.cpp

HEADERS += \
    logsubsystem.h \
    coreserver.h \
    protocolhandler.h \
    serialserver.h \
    sslclient.h # \
#    widget.h

#FORMS += \
#    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
