#ifndef SSLCLIENT_H
#define SSLCLIENT_H

#include <QSslSocket>
#include <QtSerialPort/QSerialPort>
//#include "protocollhandler.h"

class SerialServer;

class SslClient : public QObject
{
    Q_OBJECT
public:
    SslClient(QObject* parent = nullptr);
    void connectToServer(QString serverIp, unsigned short serverPort);
    void sendData(QByteArray sendbuffer);
    void setSerialServer(SerialServer *newSerialServer);

signals:
    void sslSocketStateChanged();
    void encrypted();
    void socketStateChanged(QAbstractSocket::SocketState);
    void logData(QString logEntry);

private slots:
    void readDataTcpSocket();
    void logTcpError(QAbstractSocket::SocketError error);
private:
    QSslSocket *clientSocket;
    QSerialPort *serialPort;
    QString hostName = "127.0.0.1";

    QList<QSslCertificate> ServerCert;// = QSslCertificate::fromPath(QLatin1String("./server.crt"));
    QList<QSslError> expectedSslErrors;

//    ProtocollHandler *myProtocollHandler;
    SerialServer *serialServer;
};

#endif // SSLCLIENT_H
