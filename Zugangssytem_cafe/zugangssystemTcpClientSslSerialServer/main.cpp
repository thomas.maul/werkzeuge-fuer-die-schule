//#include "widget.h"

#include <QCoreApplication>
#include <QThread>
#include "coreserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QThread *thread = new QThread;
    CoreServer *coreServer = new CoreServer;

    coreServer->moveToThread(thread);
    QObject::connect(thread, &QThread::started, coreServer, &CoreServer::run);
    thread->start();
//    Widget w;
//    w.show();    
    return a.exec();
}
