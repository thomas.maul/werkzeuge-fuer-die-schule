#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSerialPortInfo>
#include "sslclient.h"
#include "serialserver.h"
//#include "protocollhandler.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_btnConnectSsLServer_clicked();
    void stateEncrypted();
    void socketStateChanged(QAbstractSocket::SocketState socketState);

    void on_btnRequestOpenDoor_clicked();

    void logData(QString logEntry);
    void on_btnListComPorts_clicked();
    void on_btnOpenComPort_clicked();

private:
    Ui::Widget *ui;
    SslClient *sslClient;
    SerialServer *serialServer;
    QList<QSerialPortInfo> portList;
};
#endif // WIDGET_H
