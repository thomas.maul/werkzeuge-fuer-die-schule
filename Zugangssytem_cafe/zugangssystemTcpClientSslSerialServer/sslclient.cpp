#include "sslclient.h"
#include "serialserver.h"

SslClient::SslClient(QObject* parent)
    : QObject(parent)
{
//    myProtocollHandler = new ProtocollHandler;

    ServerCert = QSslCertificate::fromPath(QLatin1String("./server.crt"));
    clientSocket = new QSslSocket();
    QSslError sslError(QSslError::SelfSignedCertificate, ServerCert.at(0));
    expectedSslErrors.append(sslError);
    //dataStage = dataReadLen;

    clientSocket->setPrivateKey("client.key");
    clientSocket->setLocalCertificate("client.cert");
    clientSocket->ignoreSslErrors(expectedSslErrors);
    connect(clientSocket, &QSslSocket::encrypted, this, &SslClient::encrypted);
    connect(clientSocket, &QAbstractSocket::stateChanged, this, &SslClient::sslSocketStateChanged);
    connect(clientSocket, &QAbstractSocket::readyRead, this, &SslClient::readDataTcpSocket);
    connect(clientSocket, &QAbstractSocket::errorOccurred, this, &SslClient::logTcpError);
}

void SslClient::setSerialServer(SerialServer *newSerialServer)
{
    serialServer = newSerialServer;
}


void SslClient::connectToServer(QString serverIp, unsigned short serverPort){
    clientSocket->connectToHostEncrypted(serverIp, serverPort);

}

void SslClient::sendData(QByteArray sendbuffer)
{
    if(clientSocket->isEncrypted()){
        clientSocket->write(sendbuffer);
    }
}

/**
 * @brief lese Daten aus tcpBuffer
 *
 * @todo daten sollen per rc6 verschlüsselt vom Controller an Server übertragen werden. Hier entschlüsseln.
 */
void SslClient::readDataTcpSocket(){
    QByteArray inBuffer;
    if(clientSocket->bytesAvailable()){
        inBuffer = clientSocket->readAll();
        qDebug() << inBuffer;
        serialServer->sendData(inBuffer);
//        myProtocollHandler->computeData(inBuffer);
    }
}

void SslClient::logTcpError(QAbstractSocket::SocketError error)
{
    emit logData(clientSocket->errorString());
}

