#ifndef PROTOCOLHANDLER_H
#define PROTOCOLHANDLER_H

#include <QByteArray>
#include <QString>

class ProtocolHandler
{
public:
    ProtocolHandler();
    void computeData(QByteArray incommingData);
    enum dataReadStage{dataReadLen, dataReadCommand, dataReadParameter};
private:
    dataReadStage dataStage;
    unsigned char dataLen;
    unsigned char requestedCommand;
    unsigned int cardId;
    unsigned char replay;

    bool readLen();
    bool readCommand();
    bool readParameters();
    QByteArray inBuffer;
};

#endif // PROTOCOLHANDLER_H
