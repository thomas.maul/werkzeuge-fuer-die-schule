#ifndef SERIALSERVER_H
#define SERIALSERVER_H

#include <QObject>
#include <QSerialPort>
#include <QDataStream>
#include <QDebug>
//#include "protocolhandler.h"

class SslClient;

class SerialServer : public QObject
{
    Q_OBJECT
public:
    SerialServer(QObject* parent = nullptr);
    void openComPort(QSerialPortInfo serialPortInfo);
    void setSslClient(SslClient *newSlClient);
    void sendData(QByteArray sendBuffer);
    enum dataReadStage{dataReadLen, dataReadDataPart, dataReadDataReady};
signals:
    void logData(QString logEntry);
private slots:
    void readDataSerialSocket();
private:
    dataReadStage dataStage;
    QSerialPort *serialPort;
    SslClient *sslClient;
    QByteArray inBuffer;
    QByteArray outBuffer;
    //    ProtocolHandler *myProtocolHandler;
    int dataLen;

    void computeData();
    bool readLen();
    bool readData();
    void sendData();
};

#endif // SERIALSERVER_H
