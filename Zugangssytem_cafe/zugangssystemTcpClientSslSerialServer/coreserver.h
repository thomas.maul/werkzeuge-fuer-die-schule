#ifndef CORESERVER_H
#define CORESERVER_H

#include <QObject>
#include <QSerialPortInfo>

#include "serialserver.h"
#include "sslclient.h"
#include "logsubsystem.h"

class CoreServer : public QObject
{
    Q_OBJECT
public:
    CoreServer(QObject* parent = nullptr);
    void listComPorts();
    void openComPort(int selectedPort);
public slots:
    void run();
signals:
    void logData(QString logEntry);
private slots:
//    void logData(QString logEntry);
    void stateEncrypted();
private:
    SslClient *sslClient;
    SerialServer *serialServer;
    LogSubsystem *logSubsystem;
    QList<QSerialPortInfo> portList;
    QString serverIp;
    unsigned short serverPort;
};

#endif // CORESERVER_H
