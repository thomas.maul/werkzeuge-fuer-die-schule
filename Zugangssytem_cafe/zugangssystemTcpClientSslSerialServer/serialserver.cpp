#include "serialserver.h"
#include "qserialportinfo.h"
#include "sslclient.h"

SerialServer::SerialServer(QObject* parent)
    : QObject(parent)
{
    //    myProtocolHandler = new ProtocolHandler;
    serialPort = new QSerialPort;
    connect(serialPort, &QIODevice::readyRead, this, &SerialServer::readDataSerialSocket);
    sslClient= nullptr;
    dataStage = dataReadLen;
    dataLen = 0;
}

void SerialServer::setSslClient(SslClient *newSlClient){
    sslClient = newSlClient;
}

void SerialServer::openComPort(QSerialPortInfo serialPortInfo)
{
    serialPort->setPort(serialPortInfo);
    serialPort->setFlowControl(QSerialPort::NoFlowControl);
    serialPort->setParity(QSerialPort::NoParity);

    serialPort->open(QIODevice::ReadWrite);
    if(serialPort->isOpen())
    {
        emit logData("port open");

        serialPort->setDataTerminalReady(true);
    }
}

void SerialServer::sendData(QByteArray sendBuffer)
{
    serialPort->write(sendBuffer);
}

void SerialServer::readDataSerialSocket(){
    if(serialPort->bytesAvailable()){
        inBuffer.append(serialPort->readAll());

        // todo: decrypt rc6
    }
    computeData();
}

void SerialServer::computeData(){
    switch (dataStage) {
    case dataReadLen:
        if(readLen()){
            dataStage = dataReadDataPart;
            if(readData()){
                dataStage = dataReadDataReady;
                sendData();
                dataStage = dataReadLen;
            }
        }
        break;
    case dataReadDataPart:
        if(readData()){
            dataStage = dataReadDataReady;
            sendData();
            dataStage = dataReadLen;
        }
        break;

    case dataReadDataReady:
        sendData();
        dataStage = dataReadLen;
        break;
    }
}

bool SerialServer::readLen(){

    if(!inBuffer.isEmpty()){
        dataLen = inBuffer.at(0);
        outBuffer.append(dataLen);
        inBuffer.remove(0,1);
        return true;
    }
    return false;
}

bool SerialServer::readData(){
    if(inBuffer.size() >= dataLen){
        outBuffer.append(inBuffer.left(dataLen));
        inBuffer.remove(0,dataLen);
        return true;
    }
    return false;
}

void SerialServer::sendData(){
    QString outString;

    sslClient->sendData(outBuffer);

    for(int i = 0; i < outBuffer.size(); i++)
        outString.append(QString::number(outBuffer[i], 16) + " ");
    emit logData(outString);
    outBuffer.clear();
}
