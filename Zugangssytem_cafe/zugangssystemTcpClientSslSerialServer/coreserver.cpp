#include "coreserver.h"

CoreServer::CoreServer(QObject *parent)
    : QObject(parent)
{
    logSubsystem = new LogSubsystem(this);
    sslClient = new SslClient(this);
    serialServer = new SerialServer(this);

    serialServer->setSslClient(sslClient);
    sslClient->setSerialServer(serialServer);

    serverIp = "127.0.0.1";
    serverPort = 33443;

    connect(sslClient, &SslClient::encrypted, this, &CoreServer::stateEncrypted);
    //    connect(sslClient, &SslClient::socketStateChanged, this, &Widget::socketStateChanged);
    QObject::connect(this, &CoreServer::logData, logSubsystem, &LogSubsystem::logData);
    QObject::connect(sslClient, &SslClient::logData, this, &CoreServer::logData);
    QObject::connect(serialServer, &SerialServer::logData, this, &CoreServer::logData);
}

void CoreServer::run()
{
    sslClient->connectToServer(serverIp, serverPort);
    listComPorts();
    openComPort(0);
}

void CoreServer::stateEncrypted(){
    emit logData("encrypted connection established");
}

//void CoreServer::logData(QString logEntry)
//{
//    emit logData(logEntry);
//}

void CoreServer::listComPorts()
{
    portList = QSerialPortInfo::availablePorts();

    for(int i = 0; i < portList.size(); i++)
    {
        emit logData(QString::number(i) + " " + portList[i].portName() + " " + portList[i].description());
    }
}

void CoreServer::openComPort(int selectedPort)
{
    if(!portList.isEmpty())
    {
        serialServer->openComPort(portList[selectedPort]);
    }
}

