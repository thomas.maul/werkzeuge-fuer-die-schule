#include "protocolhandler.h"

ProtocolHandler::ProtocolHandler()
{
    dataStage = dataReadLen;
}

void ProtocolHandler::computeData(QByteArray incommingData){
    inBuffer += incommingData;

    switch (dataStage) {
    case dataReadLen:
        if(readLen()){
            dataStage = dataReadCommand;
            if(readCommand()){
                dataStage = dataReadParameter;
                if(readParameters()){
                    dataStage = dataReadLen;
                }
            }
        }
        break;
    case dataReadCommand:
        if(readCommand()){
            dataStage = dataReadParameter;
            if(readParameters()){
                dataStage = dataReadLen;
            }
        }
        break;

    case dataReadParameter:
        if(readParameters()){
            dataStage = dataReadLen;
        }
        break;
    }
}

bool ProtocolHandler::readLen(){

    if(!inBuffer.isEmpty()){
        dataLen = inBuffer.at(0);
        inBuffer.remove(0,1);
        return true;
    }
    return false;
}

bool ProtocolHandler::readCommand(){

    if(!inBuffer.isEmpty()){
        requestedCommand = inBuffer.at(0);
        inBuffer.remove(0,1);
        return true;
    }
    return false;
}

bool ProtocolHandler::readParameters(){
    //    QByteArray tempArray;
    QString logString;
    unsigned char doorState = 0;

    switch (requestedCommand) {
    case 0x28: // RFID -> Server
        if(inBuffer.size()>=5){
            //            tempArray =         if(inBuffer.size() >= (dataLen - 1))
            cardId = (unsigned char) inBuffer.at(0);
            cardId <<= 8;
            cardId += (unsigned char) inBuffer.at(1);
            cardId <<= 8;
            cardId += (unsigned char) inBuffer.at(2);
            cardId <<= 8;
            cardId += (unsigned char) inBuffer.at(3);
            replay = inBuffer[4];
            logString = "new request: open door. Card ID: " + QString::number(cardId, 16);
//            emit logData(logString);

//            if(parent->openDoor(cardId)){
//                sendReplyOpenDoor();
//            }
            inBuffer.remove(0,5);
            return true;
        }
        break;
    case 0x31: // Server -> Door
        if(inBuffer.size()>=4){
            //            tempArray =         if(inBuffer.size() >= (dataLen - 1))
            cardId = (unsigned char) inBuffer.at(0);
            cardId <<= 8;
            cardId += (unsigned char) inBuffer.at(1);
            cardId <<= 8;
            cardId += (unsigned char) inBuffer.at(2);
            cardId <<= 8;
            cardId += (unsigned char) inBuffer.at(3);
            logString = "welcome card ID: " + QString::number(cardId, 16);
//            emit logData(logString);

            inBuffer.remove(0,4);
            return true;
        }
        break;
    case 0x39:
        if(inBuffer.size() >= 1)
            doorState = inBuffer[0];
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "new reply: door state " + QString::number(doorState, 16);
//        emit logData(logString);
        return true;
        break;
    case 0x58:
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "dummy: Controller status ";
//        emit logData(logString);
        return true;
        break;
    case 0x5A:
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "dummy: RFID status ";
//        emit logData(logString);
        break;
    case 0x5B:
        if(inBuffer.size() >= 1)
            doorState = inBuffer[0];
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "new status: door state " + QString::number(doorState, 16);
//        emit logData(logString);
        return true;
        break;
    default:
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "dummy; unknown data read";
//        emit logData(logString);
        return true;
        break;
    }
    return false;
}
