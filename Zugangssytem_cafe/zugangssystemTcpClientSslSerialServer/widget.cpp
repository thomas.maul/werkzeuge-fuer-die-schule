#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    sslClient = new SslClient(this);
    serialServer = new SerialServer(this);

    serialServer->setSslClient(sslClient);
    sslClient->setSerialServer(serialServer);

    connect(sslClient, &SslClient::encrypted, this, &Widget::stateEncrypted);
    connect(sslClient, &SslClient::socketStateChanged, this, &Widget::socketStateChanged);
    connect(sslClient, &SslClient::logData, this, &Widget::logData);
    connect(serialServer, &SerialServer::logData, this, &Widget::logData);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_btnConnectSsLServer_clicked()
{
    sslClient->connectToServer(ui->edtServerIp->text(), ui->spinServerPort->value());
}

void Widget::stateEncrypted(){
    ui->btnConnectSsLServer->setStyleSheet("background-color: rgb(0, 255, 0);");
}

void Widget::socketStateChanged(QAbstractSocket::SocketState socketState){

    switch (socketState) {
    case QAbstractSocket::ConnectingState:
        ui->edtLog->appendPlainText("connecting");
        break;
    case QAbstractSocket::ConnectedState:
        ui->edtLog->appendPlainText("connected successful");
        break;

    default:
        break;
    }
}

void Widget::on_btnRequestOpenDoor_clicked()
{
    QByteArray testArray;
    testArray.append(0x06);
    testArray.append(0x28);
    testArray.append(0xAA);
    testArray.append(0xBB);
    testArray.append(0xCC);
    testArray.append(0xDD);
    testArray.append(0x01);
    sslClient->sendData(testArray);
}

void Widget::logData(QString logEntry)
{
    ui->edtLog->appendPlainText(logEntry);
}

void Widget::on_btnListComPorts_clicked()
{
    portList = QSerialPortInfo::availablePorts();

    ui->listSerialPorts->clear();
    for(int i = 0; i < portList.size(); i++)
    {
        ui->listSerialPorts->addItem(QString::number(i) + " " + portList[i].portName() + " " + portList[i].description());
    }
    //    ui->btnOpenComPort->setEnabled(true);
}

void Widget::on_btnOpenComPort_clicked()
{
    if(!portList.isEmpty())
    {
        serialServer->openComPort(portList[ui->listSerialPorts->currentRow()]);
    }
}

