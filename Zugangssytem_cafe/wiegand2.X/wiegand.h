#ifndef _WIEGAND_H
#define _WIEGAND_H

#include <stdio.h>
#include <stdlib.h>
//#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>

void initWiegand();
unsigned char WIEGAND_available();
unsigned long WIEGAND_getCode();
int WIEGAND_getWiegandType();
unsigned long WIEGAND_GetCardId (volatile unsigned long *codehigh, volatile unsigned long *codelow, char bitlength);
unsigned char WIEGAND_DoWiegandConversion ();

#endif
