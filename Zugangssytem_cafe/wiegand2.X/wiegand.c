#include "wiegand.h"

volatile unsigned long _cardTempHigh=0;
volatile unsigned long _cardTemp=0;
volatile unsigned long _lastWiegand=0;
unsigned long _code=0;
volatile int _bitCount=0;	
int _wiegandType=0;


unsigned long WIEGAND_getCode()
{
	return _code;
}

int WIEGAND_getWiegandType()
{
	return _wiegandType;
}

unsigned char WIEGAND_available()
{
	unsigned char ret;
    cli();
	ret=WIEGAND_DoWiegandConversion();
    sei();
	return ret;
}
/** initWiegand
 * 
 * setze Int0, Int1 beide auf falling edge
 * Pins: PD2 (WIEGAND_D0), PD3 (WIEGAND_D1)
 */

void initWiegand()
{
	_lastWiegand = 0;
	_cardTempHigh = 0;
	_cardTemp = 0;
	_code = 0;
	_wiegandType = 0;
	_bitCount = 0;  
	
    DDRD &= ~((1 << PD2) | (1 << PD3));
    sei();
	
	//attachInterrupt(digitalPinToInterrupt(pinD0), ReadD0, FALLING);  // Hardware interrupt - high to low pulse
	//attachInterrupt(digitalPinToInterrupt(pinD1), ReadD1, FALLING);  // Hardware interrupt - high to low pulse
}

ISR(INT0_vect)// WIEGAND_ReadD0 ()
{
	_bitCount++;				// Increament bit count for Interrupt connected to D0
	if (_bitCount>31)			// If bit count more than 31, process high bits
	{
		_cardTempHigh |= ((0x80000000 & _cardTemp)>>31);	//	shift value to high bits
		_cardTempHigh <<= 1;
		_cardTemp <<=1;
	}
	else
	{
		_cardTemp <<= 1;		// D0 represent binary 0, so just left shift card data
	}
	
    TCCR1B &= ((1 << CS12) | (1 << CS10));// Timer on
    TCCR1B &= ~((1 << CS11));// Timer on
}

ISR(INT1_vect) //void WIEGAND_ReadD1()
{
	_bitCount ++;				// Increment bit count for Interrupt connected to D1
	if (_bitCount>31)			// If bit count more than 31, process high bits
	{
		_cardTempHigh |= ((0x80000000 & _cardTemp)>>31);	// shift value to high bits
		_cardTempHigh <<= 1;
		_cardTemp |= 1;
		_cardTemp <<=1;
	}
	else
	{
		_cardTemp |= 1;			// D1 represent binary 1, so OR card data with 1 then
		_cardTemp <<= 1;		// left shift card data
	}
	
    TCCR1B &= ((1 << CS12) | (1 << CS10));// Timer on
    TCCR1B &= ~((1 << CS11));// Timer on
}

unsigned long WIEGAND_GetCardId (volatile unsigned long *codehigh, volatile unsigned long *codelow, char bitlength)
{
	if (bitlength==26)								// EM tag
		return (*codelow & 0x1FFFFFE) >>1;

	if (bitlength==24)
		return (*codelow & 0x7FFFFE) >>1;

	if (bitlength==34)								// Mifare 
	{
		*codehigh = *codehigh & 0x03;				// only need the 2 LSB of the codehigh
		*codehigh <<= 30;							// shift 2 LSB to MSB		
		*codelow >>=1;
		return *codehigh | *codelow;
	}

	if (bitlength==32) {
		return (*codelow & 0x7FFFFFFE ) >>1;
	}

	return *codelow;								// EM tag or Mifare without parity bits
}

char WIEGAND_translateEnterEscapeKeyPress(char originalKeyPress) {
	switch(originalKeyPress) {
	case 0x0b:        // 11 or * key
		return 0x0d;  // 13 or ASCII ENTER

	case 0x0a:        // 10 or # key
		return 0x1b;  // 27 or ASCII ESCAPE

	default:
		return originalKeyPress;
	}
}

unsigned char WIEGAND_DoWiegandConversion ()
{
	unsigned long cardID;
	
	if (_lastWiegand > 25)								// if no more signal coming through after 25ms
	{
        TCCR1B &= ~((1 << CS12) | (1 << CS11) | (1 << CS10));// Timer off
        _lastWiegand = 0;
        
		if ((_bitCount==24) || (_bitCount==26) || (_bitCount==32) || (_bitCount==34) || (_bitCount==8) || (_bitCount==4)) 	// bitCount for keypress=4 or 8, Wiegand 26=24 or 26, Wiegand 34=32 or 34
		{
			_cardTemp >>= 1;			// shift right 1 bit to get back the real value - interrupt done 1 left shift in advance
			if (_bitCount>32)			// bit count more than 32 bits, shift high bits right to make adjustment
				_cardTempHigh >>= 1;

			if (_bitCount==8)		// keypress wiegand with integrity
			{
				// 8-bit Wiegand keyboard data, high nibble is the "NOT" of low nibble
				// eg if key 1 pressed, data=E1 in binary 11100001 , high nibble=1110 , low nibble = 0001 
				char highNibble = (_cardTemp & 0xf0) >>4;
				char lowNibble = (_cardTemp & 0x0f);
				_wiegandType=_bitCount;					
				
				
				if (lowNibble == (~highNibble & 0x0f))		// check if low nibble matches the "NOT" of high nibble.
				{
					_code = (int)WIEGAND_translateEnterEscapeKeyPress(lowNibble);
					return 1;
				}
				else {
					_bitCount=0;
					_cardTemp=0;
					_cardTempHigh=0;
					return 0;
				}

				// TODO: Handle validation failure case!
			}
			else if (4 == _bitCount) {
				// 4-bit Wiegand codes have no data integrity check so we just
				// read the LOW nibble.
				_code = (int)WIEGAND_translateEnterEscapeKeyPress(_cardTemp & 0x0000000F);

				_wiegandType = _bitCount;
				_bitCount = 0;
				_cardTemp = 0;
				_cardTempHigh = 0;
				return 1;
			}
			else		// wiegand 26 or wiegand 34
			{
				cardID = WIEGAND_GetCardId (&_cardTempHigh, &_cardTemp, _bitCount);
				_wiegandType=_bitCount;
				_bitCount=0;
				_cardTemp=0;
				_cardTempHigh=0;
				_code=cardID;
				return 1;
			}
		}
		else
		{
			// well time over 25 ms and bitCount !=8 , !=26, !=34 , must be noise or nothing then.
			_bitCount=0;			
			_cardTemp=0;
			_cardTempHigh=0;
			return 0;
		}	
	}
	else
	return 0;
}

/** Timer fuer misec
 * 
 * prescaler muss auf 1024 (TCCR1B : CS12 = 1, CS11 = 0; CS10 = 1)
 * werden in iNT= und INT1 gesetzt (Bit ist angekommen)
 */
ISR(TIMER1_OVF_vect)
{
    _lastWiegand++;
    TCNT1 = 49910; //diff to max. width prescaler = 1024 -> 1mS
}