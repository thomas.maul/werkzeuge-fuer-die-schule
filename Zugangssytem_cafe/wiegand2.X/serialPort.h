/* 
 * File:   serialPort.h
 * Author: thomas
 *
 * Created on 13. August 2022, 14:35
 */

#ifndef SERIALPORT_H
#define	SERIALPORT_H

#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>

unsigned char lastRequest;
unsigned long lastCardId;
unsigned char openTime; // max 4 min
unsigned char cardIdRepeat;

void initUart();
void sendRequest(unsigned long cardId);
unsigned char readLastRequest();

#endif	/* SERIALPORT_H */

