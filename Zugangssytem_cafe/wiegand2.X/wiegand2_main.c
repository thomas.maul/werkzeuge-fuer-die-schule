/* 
 * File:   wiegand2_main.c
 * Author: thomas
 *
 * Created on 13. August 2022, 09:21
 */
#define F_CPU 8000000UL
#include <util/delay.h>

#include "wiegand.h"
#include "serialPort.h"
unsigned char cardIdRepeat = 1;

void initStatusLed(){
    //init LED
    DDRB |= (1 << PB5);
    PORTB |= (1 << PB5);
}

/**
 * pb0 = door opener (engine lock)
 * pb1 = led signaling status (in sync to pb0)
 */
void initDoorOut(){
    DDRB |= (1 << PB0); // door
    DDRB |= (1 << PB1); // LED
    PORTB &= ~(1 << PB0);
    PORTB &= ~(1 << PB1);
}

/**
 * open the door, switch indicator LED on
 * indicator LED is 2 sec delay to engine on
 * opentime: 20 sec
 */
void openDoor(){
    PORTB |= (1 << PB0);
    _delay_ms(2000); // warten, bis Schloss offen ist
    PORTB |= (1 << PB1);
    _delay_ms(18000);
    PORTB &= ~(1 << PB1);
    PORTB &= ~(1 << PB0);
}

/**
 * 
 */
int main(int argc, char** argv) {
//    unsigned char inByte = 0x00;
    initUart();
    initWiegand();
    initStatusLed();
    initDoorOut();
    
    unsigned long int cardId = 0x2B68D043;//0xAABBCCDD;
        
    while(1){
        if(WIEGAND_available()){
            cardId = WIEGAND_getCode();
            
            WIEGAND_getWiegandType();
            sendRequest(cardId);
        }   
        
        //dummy card aa bb cc dd
        if(!(PINB & (1 << PB7))&&(cardId)){
            //0xAABBCCDD;
            sendRequest(cardId);
            cardId = 0x00;
        }
            
        if(1 == readLastRequest()){
            openDoor();
        }
    }
    
    return (EXIT_SUCCESS);
}










