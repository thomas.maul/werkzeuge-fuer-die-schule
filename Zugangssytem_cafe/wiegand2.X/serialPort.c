
#include "serialPort.h"

volatile unsigned char serialInBuffer[50];
//volatile unsigned char serialInBufCounter;
volatile unsigned char serialInBufReadPos = 0;
volatile unsigned char serialInBufWritePos = 0;

unsigned char currentRequestLen;
unsigned char lastRequestComplete = 1;

unsigned char currentRequestCommand = 0;
unsigned long currentRequestCardId = 0;

unsigned char bytesAvailable = 0;
unsigned char freshSerialDataAvailable = 0;

unsigned char inByte = 0;

unsigned char readDataFromSerialInBuffer(unsigned char command);

void initUart(){
    
    UBRR0H = 0x00;
    UBRR0L = 0x67;
    UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); // 8bit
}
/**
 * send Data to Pi
 * request: 0x28 (command); cardId (parameter); repeat (0/1)
 * 
 * repeat 0 = new, 1 = repeating
 * @param cardId Id fron RFID-Card
 */
void sendRequest(unsigned long cardId) {
    unsigned char sendChar = 0;

    //len
    while (!(UCSR0A & (1 << UDRE0))) {
    }
    UDR0 = 0x06; //len of(command + cardId + repeat)

    //command
    while (!(UCSR0A & (1 << UDRE0))) {
    }
    UDR0 = 0x28;

    //cardId
    for (int i = 3; i >= 0; i--) {
        while (!(UCSR0A & (1 << UDRE0))) {
        }

        sendChar = (unsigned char) (0xFF & (cardId >> i*8));
        UDR0 = sendChar;
    }

    //repeat
    while (!(UCSR0A & (1 << UDRE0))) {
    }
    UDR0 = cardIdRepeat;

    cardIdRepeat = 2;
}

/**
 * read a byte from incomming buffer (serial port)
 *
 * @return status 1= ok, 0 = nok
 */
unsigned char readByteFromInBuffer(){
    
    if(bytesAvailable>=1){
        inByte= serialInBuffer[serialInBufReadPos];
        serialInBuffer[serialInBufReadPos] = 0;
        serialInBufReadPos++;
        if (serialInBufReadPos >= 50)
            serialInBufReadPos = 0;
        bytesAvailable--;
        currentRequestLen--;
        return 1;
    }
    return 0;
}

/**
 * readRequest
 * @return 1 = ok, ff = nok, fe = not enough data available
 * 0xfd = no new data available
 */
unsigned char readLastRequest() {
    unsigned char retValue = 0xff;
    

    if (!freshSerialDataAvailable)
        return 0xfd;
    else {
        freshSerialDataAvailable = 0;

        if (lastRequestComplete) {
            if(1 == readByteFromInBuffer(inByte)){
                currentRequestLen = inByte;
                lastRequestComplete = 0;
            }
        }

        if (bytesAvailable >= currentRequestLen) {
            if(1 == readByteFromInBuffer(inByte))
            currentRequestCommand = inByte;
            readDataFromSerialInBuffer(currentRequestCommand);
            
            lastRequestComplete = 1;
            retValue = 1;
        } else
            retValue = 0xfe;
    }
    return retValue;
}

/**
 * 
 * @param command current Request / command von Pi / PC
 * @return received value
 */
unsigned char readDataFromSerialInBuffer(unsigned char command){
    unsigned long tempValue = 0;
    
    switch(command){
        case 0x31:
            tempValue = 0;
            for(int i = 0; i < 4; i++){
                if(1 == readByteFromInBuffer(inByte))
                tempValue += inByte;

                if(i < 3) 
                    tempValue = tempValue << 8;
            }
            
            if(PINB & (1 << PB5))
                PORTB &= ~(1 << PB5);
            else    
                PORTB |= (1 << PB5);
            break;
        case 0x32:
            for(int i = 0; i < 4; i++){
                if(1 == readByteFromInBuffer(inByte))
                tempValue |= inByte;

                if(i < 3) 
                    tempValue <<= 8;
            }
            lastCardId = tempValue;
            break;
        case 0x33:
            break;
        case 0x37:
            if(1 == readByteFromInBuffer(inByte))
                openTime = inByte;
            break;
//        case 50:
//        case 52:
//        case 53:
    }
    return tempValue;
}



/**
 * rec data from Pi
 */
ISR(USART_RX_vect) {
    serialInBuffer[serialInBufWritePos] = UDR0;
    serialInBufWritePos++;
    if (serialInBufWritePos >= 50)
        serialInBufWritePos = 0;
    bytesAvailable++;
    freshSerialDataAvailable = 1;
}