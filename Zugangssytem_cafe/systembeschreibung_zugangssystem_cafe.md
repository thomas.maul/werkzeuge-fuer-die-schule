# Zugangssystem Schüler:innen Cafe

## Systemaufbau
Das System besteht aus Hardware (bei Prototyp: Arduino  (2x) und Raspberry PI (V2 - ), RFID-Leser (Wiegand32).

So wie Software:

- Programm für Arduino / Tür, 
- Programm für Arduino Einlesestation, 
- TcpServer (auf virtueller Maschine vorzugsweise Linux), 
- TcpClient (auf Raspberry PI)

Das System ist nicht gegen Stromausfall abgesichert. Nach einem Stromausfall, bei Systemneustart muss der Client neu mit der Tür-Station (Arduino) und dem Server verbunden werden.

### Tür
Leegerät für RFID Mifare classic Karten, Motorschloss.

Motorschloss muss Panikfunktion haben (von innen immer mit Klinke zu öffnen sein). Soll Einschub für physikalischen Zylinder haben (Öffnen bei Systemstörung, Stromausfall).
Öffnung erfolgt, wenn Spannung anliegt.
- Reader Mifare classic (13.56 MHz) mit Wiegand 32 Protokoll
- Motorschloss

### Steuerung Tür
- Controller (ATMega328p) z.B. Arduino Uno (R3)
- Anschluss RFID Leser (via Wiegand32) D0: Pin2, D1: Pin3 
- Versorgung des Lesers benötigt 12V! extern bereitstellen (eigenes Netzteil)
- Anschluss Motorschloss via Relais, extern bereitstellen (eigenes Netzteil)

### Vermittlerstation
Im Cafe, in geschlossenen Gehäuse sind Arduino / Tür, Raspberry Pi (TcpClient), beide sind mit USB-Kabel (A-B) verbunden.

- Raspberry Pi oder ähnlich (V2 -)
- Software TcpClient
- Daten für Server eintragen
  - IP-Adresse
  - Zertifikat vom Server
- Anbindung an Server 
  - via VLAN (exklusiv), 
  - alternativ im LAN, 
  - wenn WLAN eigene SSID verwenden, von übrigem WLAN abtrennen.
  - wenn Kopplung über Internet notwendig: VPN (möglichst exklusiv) verwenden.
  
### Server
- Virtuelle Maschine im Serverraum
- TcpServer
- Datenbank 1: mrbs (Software: https://mrbs.sourceforge.io/)
- Datenbank als PostgreSQl (MariaDB unter Linux möglich, Server umkonfigurieren)
- Datenbank 2: lokale Daten Tür PostgreSQL
- Daten für Server eintragen
  - Zertifikat und private key vom Server
  - Zugangsdaten zu Datenbank 1 und 2.
  
### Einlesestation
- Arduino Uno R3
- Software (BSP->MFRC522->ReadNUID alt: github:https://github.com/miguelbalboa/rfid -> examples->ReadNUID)


## Entwicklungssystem, Treiber

### Software, Treiber für Arduinio / RFID / Wiegand
- https://github.com/monkeyboard/Wiegand-Protocol-Library-for-Arduino
- https://gitlab.com/thomas.maul/werkzeuge-fuer-die-schule/-/tree/main/Zugangssytem_cafe
- wiegand2.X
- Entwicklungsumgebung: Microchip/MPLab
  
### Software TcpClient
Koppelt seriell (via USB) mit Server (Via TCP/TLS)
- https://gitlab.com/thomas.maul/werkzeuge-fuer-die-schule/-/tree/main/Zugangssytem_cafe
-  zugangssystemTcpClientSslSerialServer
- Entwicklungsumgebung: Qt (V 5.X, wenn V 6 seriel unterstützt dann V 6)

### Software TcpServer
auch Für Datenbankzugriffe
-  zugangssystemTcpServerSsl
- Entwicklungsumgebung: Qt (V 6.X)

### Raspberry OS
- System härten: keine Server aktiv, nur SSH für remote login.
- Firewall aktivieren.
- Updates auf unattended automatisch setzen.
- Logs ausschalten oder in Ram Disk, gebündelt auf USB-Stick.
- mechanischen Zugriff begrenzen (Arduino und Pi in ein ausreichend großes Gehäuse (Abwärme).

## Wartung
regelmäßig kontrollieren:

### in der Regel monatlich
- Logs (min monatlich)
- Updates (min monatlich)
- Ablaufdatum des Zertifikats Server

### in der Regel jährlich
- Zertifikat für Server erneuern, bei Client einspielen, Kommunikation testen
- Datenbank aufräumen (alte Einträge löschen)
