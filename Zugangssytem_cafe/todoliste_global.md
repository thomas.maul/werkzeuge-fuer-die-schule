# Todoliste Zugangssystem Cafe

## TcpServer
- [ ] Daten für Datenbank (Login) in Datei schreiben (conf-Datei)
- [ ] Code aufräumen
- [ ] Code / Methoden dokumentieren
- [ ] Clientzertifikat anfordern und prüfen

## TcpClient
- [ ] Daten für Zugang (IP, ... von server) in Conf-Datei
- [ ] Code aufräumen
- [ ] Code / Methoden dokumentieren
- [ ] Clientzertifikat erstellen
- [ ] Clientzertifikat einbinden (Vorweisen gegenüber Server)


## Datenbank Zugangssystem
- [ ] Rechte prüfen
- [ ] Script für Neuanlegen (mit Rechten) erstellen
  - [ ] Tabellen anlegen
  - [ ] Passwort für Userkonto Zugang generieren
  - [ ] Userkonto Zugang anlegen
  - [ ] Rechte für Userkonto Zugang festlegen
  - [ ] tadID verschlüsselt ablegen (hash mit salt)

## Datenbank mrbs
- [ ] Anleitung für Einrichtung mit langen kyptischen PW erstellen.
- [ ] Passwort für Userkonto Zugang generieren
- [ ] Userkonto Zugang anlegen
- [ ] Rechte für Userkonto Zugang festlegen

## Einlesestation
- [ ] Programm für eintragen in DB Zugangssystem erstellen
- [ ] Programm für eintragen in DB Zugangssystem testen

## Raspberry Pi
- [ ] Unattended Upgrades einrichten https://wiki.debian.org/UnattendedUpgrades
- [ ] Logs einstellen, dass sie nur alle Minute auf USB-Stick kommen

## Installation Server
- [ ] Installation auf verschlüsseltes System
- [ ] Zugriffsrechte prüfen, ggf anpassen. minimale Rechte
- [ ] Installation prüfen, nur notwendige Pakete installieren
- [ ] unattended updates einschalten
