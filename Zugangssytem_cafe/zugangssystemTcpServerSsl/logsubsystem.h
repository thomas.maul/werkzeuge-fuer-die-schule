#ifndef LOGSUBSYSTEM_H
#define LOGSUBSYSTEM_H

#include <QObject>
#include <QDateTime>

class LogSubsystem : public QObject
{
    Q_OBJECT
public:
    LogSubsystem(QObject* parent = nullptr);

public slots:
    void logData(QString logString);
    void logServerListenStatus(bool logStatus);
};

#endif // LOGSUBSYSTEM_H
