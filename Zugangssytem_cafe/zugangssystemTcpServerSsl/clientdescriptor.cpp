#include "clientdescriptor.h"
#include "server.h"

ClientDescriptor::ClientDescriptor(Server *parent)
    : QObject(parent)
{
    this->parent = parent;
    clientSocket = nullptr;
    dataStage = dataReadLen;
}

ClientDescriptor::ClientDescriptor(QSslSocket *newClientSocket, Server *parent)
    : QObject(parent)
{
    this->parent = parent;
    clientSocket = newClientSocket;
    dataStage = dataReadLen;
    connect(clientSocket, &QTcpSocket::readyRead, this, &ClientDescriptor::readDataTcpSocket);
}

//QSslSocket *ClientDescriptor::getClientSocket() const
//{
//    return clientSocket;
//}

/**
 * @brief lese Daten aus tcpBuffer
 *
 * @todo daten sollen per rc6 verschlüsselt vom Controller an Server übertragen werden. Hier entschlüsseln.
 */
void ClientDescriptor::readDataTcpSocket(){
    if(clientSocket->bytesAvailable()){
        inBuffer.append(clientSocket->readAll());

        // todo: decrypt rc6
    }
    computeData();
}

/**
 * @brief interner test
 * @param dummyData Array mit Daten (QByteArray)
 *
 * Die Methode simuliert Daten, die eingelesen wurden.
 *
 * Zzt. nur vollständige Datensätze.
 */
//void ClientDescriptor::readDataDummy(QByteArray dummyData){
//    inBuffer.append(dummyData);
//    computeData();
//}

void ClientDescriptor::computeData(){
    switch (dataStage) {
    case dataReadLen:
        if(readLen()){
            dataStage = dataReadCommand;
            if(readCommand()){
                dataStage = dataReadParameter;
                if(readParameters()){
                    dataStage = dataReadLen;

                }
            }
        }
        break;
    case dataReadCommand:
        if(readCommand()){
            dataStage = dataReadParameter;
            if(readParameters()){
                dataStage = dataReadLen;
            }
        }
        break;

    case dataReadParameter:
        if(readParameters()){
            dataStage = dataReadLen;
        }
        break;
    }
}

bool ClientDescriptor::readLen(){

    if(!inBuffer.isEmpty()){
        dataLen = inBuffer.at(0);
        inBuffer.remove(0,1);
        return true;
    }
    return false;
}

bool ClientDescriptor::readCommand(){

    if(!inBuffer.isEmpty()){
        requestedCommand = inBuffer.at(0);
        inBuffer.remove(0,1);
        return true;
    }
    return false;
}

bool ClientDescriptor::readParameters(){
    //    QByteArray tempArray;
    QString logString;
    unsigned char doorState = 0;

    switch (requestedCommand) {
    case 0x28:
        if(inBuffer.size()>=5){
            //            tempArray =         if(inBuffer.size() >= (dataLen - 1))
            cardId = (unsigned char) inBuffer.at(0);
            cardId <<= 8;
            cardId += (unsigned char) inBuffer.at(1);
            cardId <<= 8;
            cardId += (unsigned char) inBuffer.at(2);
            cardId <<= 8;
            cardId += (unsigned char) inBuffer.at(3);
            replay = inBuffer[4];
            logString = "new request: open door. Card ID: " + QString::number(cardId, 16);
            emit logData(logString);

            if(parent->openDoor(cardId)){
                sendReplyOpenDoor();
            }
            inBuffer.remove(0,5);
            return true;
        }
        break;
    case 0x39:
        if(inBuffer.size() >= 1)
            doorState = inBuffer[0];
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "new reply: door state " + QString::number(doorState, 16);
        emit logData(logString);
        return true;
        break;
    case 0x58:
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "dummy: Controller status ";
        emit logData(logString);
        return true;
        break;
    case 0x5A:
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "dummy: RFID status ";
        emit logData(logString);
        break;
    case 0x5B:
        if(inBuffer.size() >= 1)
            doorState = inBuffer[0];
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "new status: door state " + QString::number(doorState, 16);
        emit logData(logString);
        return true;
        break;
    default:
        if(inBuffer.size() >= (dataLen - 1))
            inBuffer.remove(0,dataLen - 1);
        logString = "dummy; unknown data read";
        emit logData(logString);
        return true;
        break;
    }
    return false;
}

unsigned char ClientDescriptor::getReplay() const
{
    return replay;
}

void ClientDescriptor::disconnectClient()
{
    if(nullptr != clientSocket)
        clientSocket->disconnectFromHost();
}

unsigned int ClientDescriptor::getCardId() const
{
    return cardId;
}

unsigned char ClientDescriptor::getRequestedCommand() const
{
    return requestedCommand;
}


void ClientDescriptor::sendReplyOpenDoor(){
    QByteArray sendBuffer;
    QString logString;

    sendBuffer.append(0x04);
    sendBuffer.append(0x31);
    sendBuffer.append(inBuffer.at(0));
    sendBuffer.append(inBuffer.at(1));
    sendBuffer.append(inBuffer.at(2));
    sendBuffer.append(inBuffer.at(3));
    if(clientSocket)
        clientSocket->write(sendBuffer);

    cardId = (unsigned char) inBuffer.at(0);
    cardId <<= 8;
    cardId += (unsigned char) inBuffer.at(1);
    cardId <<= 8;
    cardId += (unsigned char) inBuffer.at(2);
    cardId <<= 8;
    cardId += (unsigned char) inBuffer.at(3);
    logString = "send open";
    emit logData(logString);
}
