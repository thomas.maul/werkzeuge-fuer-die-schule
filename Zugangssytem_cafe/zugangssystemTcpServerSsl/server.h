#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include "SslServer.h"
#include "clientdescriptor.h"

class Server : public QObject
{
    Q_OBJECT
public:
    Server(QObject* parent = nullptr);
    ~Server();
//    void startServer();
    void connectDbServerAccessSystem();
    void connectDbServerMrbs();
//    void listBookings();
    bool openDoor(unsigned long long cardId);
public slots:
    void run();


//    void sendRequestInternalTesting();
//    void clearTestArray();
//    void prepareTestRequest(short checkedId);
//    void testSendNByte(unsigned short bytesToSend);
signals:
    void serverIsListening(bool status);
    void newData();
    void logData(QString logEntry);
private slots:
    void newIpConnection();
private:
    SslServer *sslServer;
    std::vector <ClientDescriptor*> clientVector;
//    ClientDescriptor* testClient;

    int bytesSend;
    QByteArray testArray;
    QString ipAdress;
    unsigned short port;
    QSqlDatabase dbAccessSystem;
    QSqlDatabase dbMrbs;
};

#endif // SERVER_H
