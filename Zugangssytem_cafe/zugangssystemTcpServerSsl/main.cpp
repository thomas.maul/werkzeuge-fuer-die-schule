//#include "widget.h"

#include <QCoreApplication>
#include "server.h"
#include "logsubsystem.h"
//#include "SslServer.h"
//#include "clientdescriptor.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Server *mainServer;
    LogSubsystem *logSubsystem;

    QThread *thread = new QThread;
    logSubsystem = new LogSubsystem;
    mainServer = new Server;


    mainServer->moveToThread(thread);
    QObject::connect(thread, &QThread::started, mainServer, &Server::run);
    QObject::connect(mainServer, &Server::serverIsListening, logSubsystem, &LogSubsystem::logServerListenStatus);
    QObject::connect(mainServer, &Server::logData, logSubsystem, &LogSubsystem::logData);
    thread->start();
//    Widget w;
//    w.show();

    return a.exec();
}
