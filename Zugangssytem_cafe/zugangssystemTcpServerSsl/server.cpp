#include "server.h"

Server::Server(QObject* parent)
    : QObject(parent)
{
    port = 33443;
    ipAdress = "127.0.0.1";
    sslServer = nullptr;
}

Server::~Server(){
    dbAccessSystem.close();
    if(nullptr != sslServer){
        sslServer->close();
        sslServer->deleteLater();
    }
    if(0 < clientVector.size())
    for(int i = clientVector.size(); i >= 0; i--){
        clientVector.at(i)->disconnectClient();
        clientVector.at(i)->deleteLater();
        clientVector.pop_back();
    }
}

void Server::run(){
    QHostAddress hostAdress(ipAdress);

    sslServer = new SslServer;
    sslServer->setSslLocalCertificate("server.pem");
    sslServer->setSslPrivateKey("server.key");
    sslServer->setSslProtocol(QSsl::TlsV1_3);
    sslServer->setMaxPendingConnections(1);
//    testClient = nullptr;
    bytesSend = 0;
    connect(sslServer, &SslServer::newConnection, this, &Server::newIpConnection);
    if(true == sslServer->isListening()){
    }
    else{
        sslServer->listen(hostAdress, port);
        if(true == sslServer->isListening()){
            //            emit serverIsListening(true);
            emit logData("is listening on port: " +  QString::number(port));
        }
        else{
            emit serverIsListening(false);
        }
    }

    connectDbServerAccessSystem();
    connectDbServerMrbs();
}

void Server::newIpConnection(){
    QSslSocket *sslSocket = dynamic_cast<QSslSocket*>(sslServer->nextPendingConnection());
    ClientDescriptor* tempClient = new ClientDescriptor(sslSocket, this);
    clientVector.push_back(tempClient);
}


//void Server::sendRequestInternalTesting()
//{
//    if(nullptr == testClient){
//        testClient = new ClientDescriptor(nullptr, this);
//        connect(testClient, &ClientDescriptor::logData, this, &Server::logData);
//    }

//    if(nullptr != testClient){
//        testClient->readDataDummy(testArray);
//    }
//}


//void Server::testSendNByte(unsigned short bytesToSend)
//{
//    QByteArray sendArray;
//    bytesSend += bytesToSend;
//    if(testArray.size() < bytesSend)
//        bytesSend = 0;

//    if(nullptr == testClient){
//        testClient = new ClientDescriptor(nullptr, this);
//        connect(testClient, &ClientDescriptor::logData, this, &Server::logData);
//    }

//    if(nullptr != testClient){
//        sendArray = testArray.left(bytesToSend);
//        testArray.remove(0,bytesToSend);
//        testClient->readDataDummy(sendArray);
//    }
//}


//void Server::prepareTestRequest(short checkedId)
//{
//    qDebug() << checkedId;
//    switch(checkedId) {
//    case -2:
//        testArray.append(0x06);
//        testArray.append(0x28);
//        testArray.append(0xAA);
//        testArray.append(0xBB);
//        testArray.append(0xCC);
//        testArray.append(0xDD);
//        testArray.append(0x01);
//        break;
//    case -3:
//        testArray.append(0x39);
//        break;
//    case -4:
//        testArray.append(0x58);
//        break;
//    case -5:
//        testArray.append(0x5A);
//        break;
//    case -6:
//        testArray.append(0x5B);
//        break;
//    case -7:
//        testArray.append(0xFF);
//        break;
//    }
//}


//void Server::clearTestArray()
//{
//    testArray.clear();
//}


void Server::connectDbServerAccessSystem()
{
    dbAccessSystem = QSqlDatabase::addDatabase("QPSQL");//, "connAccessSystem");
    dbAccessSystem.setHostName("127.0.0.1");
    dbAccessSystem.setDatabaseName("bws_zugangssytem");
    dbAccessSystem.setUserName("bws_zugang");
    dbAccessSystem.setPassword("password1");
    bool ok = dbAccessSystem.open();

    if(true == ok){
        emit logData("db connected access");
    }
    else{
        //        qDebug() << dbAccessSystem.isValid();
        emit logData("error connecting db access");
    }
}

void Server::connectDbServerMrbs()
{
    dbMrbs = QSqlDatabase::addDatabase("QMYSQL", "connMrbs");
    dbMrbs.setHostName("127.0.0.1");
    dbMrbs.setDatabaseName("mrbs");
    dbMrbs.setUserName("bws_zugang");
    dbMrbs.setPassword("password1");
    bool ok = dbMrbs.open();

    if(true == ok){
        emit logData("db connected mrbs");
    }
    else{
        //        qDebug() << dbAccessSystem.isValid();
        emit logData("error connecting db mrbs");
    }
}


//void Server::listBookings()
//{
//    QString booking;
//    QSqlQuery query("select * from bucht", dbAccessSystem);
//    while (query.next()) {
//        booking = query.value(1).toString() + "room ";
//        booking += query.value(2).toString() + " from ";
//        booking += query.value(3).toString() + " to ";
//        booking += query.value(4).toString();
//        emit logData(booking);
//    }
//}

//select room_id, start_time, end_time, create_by from mrbs_entry
bool Server::openDoor(unsigned long long cardId)
{
//    QString currTime = QDateTime::currentDateTimeUtc().toString("yyyyMMddhhmmss");
    QString currTime = QString::number(QDateTime::currentSecsSinceEpoch());
    bool result  = false;

    QString selectString  = "SELECT public.user.mrbs_user FROM public.user, public.besitzt, public.karte";
            selectString += " WHERE public.karte.rfid = " + QString::number(cardId);
            selectString += " AND public.karte.sperrdatum IS NULL";
            selectString += " AND public.besitzt.kartennr_fk = public.karte.kartennr";
            selectString += " AND public.besitzt.userid_fk = public.user.userid;";
            qDebug() << selectString;
    //booking.append("and \"karte\".\"rfid\" = " + QString::number(cardId) + " ORDER BY  \"bucht\".\"buchungId\" DESC;");
    QSqlQuery query(selectString, dbAccessSystem);
    result = query.isActive();
    selectString.clear();
    if(query.isActive())
        qDebug() << "got sql result";
    else
        qDebug() << "no sql result";
    while (query.next()) {
        selectString = "new request, cardId: ";
        selectString += query.value(1).toString();
        qDebug() << selectString;


//        if((0 <= currTime.compare(query.value(3).toString())) && (0 >= currTime.compare(query.value(4).toString()))){
//            booking = " is valid, doors open";
//            //an ClientDescriptor: open door (Antwort auf 0x28)
//            emit logData(booking);
//            result = true;
//        }
    }
    return result;
}

