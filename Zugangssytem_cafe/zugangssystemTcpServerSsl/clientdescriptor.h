#ifndef CLIENTDESCRIPTOR_H
#define CLIENTDESCRIPTOR_H

#include <QObject>
#include <QSslSocket>

class Server;

class ClientDescriptor : public QObject
{
    Q_OBJECT
public:
    explicit ClientDescriptor(Server *parent = nullptr);
    explicit ClientDescriptor(QSslSocket *newClientSocket, Server *parent = nullptr);

    enum dataReadStage{dataReadLen, dataReadCommand, dataReadParameter};

    unsigned char getRequestedCommand() const;
    unsigned int getCardId() const;
    unsigned char getReplay() const;
    void disconnectClient();

public slots:
//    void readDataDummy(QByteArray dummyData);
signals:
    void newData();
    void logData(QString logEntry);
private slots:
    void readDataTcpSocket();
    void computeData();
private:
    Server *parent;
    QSslSocket *clientSocket;
    QByteArray inBuffer;

    dataReadStage dataStage;

    unsigned char dataLen;
    unsigned char requestedCommand;
    unsigned int cardId;
    unsigned char replay;
    bool readCommand();
    bool readLen();
    bool readParameters();
    void sendReplyOpenDoor();
};

#endif // CLIENTDESCRIPTOR_H
