#ifndef WIDGET_H
#define WIDGET_H

#include <QObject>
#include <QWidget>
#include <QMessageBox>
#include <QSslSocket>
#include <QButtonGroup>
#include <QDebug>
//#include <QSqlDatabase>
//#include <QSqlQuery>
#include "server.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    void logData(QString logEntry);
private slots:
    void startServer();

//    void newConnection();
    void on_btnSendRequestInternalTesting_clicked();
    void on_btnSendNByte_clicked();
    void on_btnPrepareRequest_clicked();
    void on_btnClearSendBuffer_clicked();
    void on_btnListDbDriver_clicked();
    
    void on_btnConnectDb_clicked();

    void on_btnDbListBookings_clicked();

    void on_pushButton_clicked();

    void on_btnOpenDoor_clicked();

    void updateServerListeningStatus(bool status);
    void on_btnTestRequestCardToDb_clicked();

private:
    Ui::Widget *ui;
    Server *mainServer;

    QButtonGroup testButtons;
};
#endif // WIDGET_H
