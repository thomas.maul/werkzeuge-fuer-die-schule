#include "widget.h"
#include "./ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget){
    ui->setupUi(this);
    mainServer = new Server(this);
    connect(ui->btnStartServer, &QPushButton::clicked, this, &Widget::startServer);
    connect(mainServer, &Server::serverIsListening, this, &Widget::updateServerListeningStatus);
    connect(mainServer, &Server::logData, this, &Widget::logData);
    mainServer->connectDbServer();
}

Widget::~Widget(){
    delete ui;
}

void Widget::logData(QString logEntry)
{
    ui->edtLog->appendPlainText(logEntry);
}

void Widget::startServer(){
    mainServer->startServer("127.0.0.1", ui->spinPort->value());
}

void Widget::on_btnSendRequestInternalTesting_clicked()
{
    mainServer->sendRequestInternalTesting();
}


void Widget::on_btnSendNByte_clicked()
{
    mainServer->testSendNByte(ui->spinSendNByte->value());
}


void Widget::on_btnPrepareRequest_clicked()
{
    qDebug() << ui->btnGrpRequests->checkedId();

    mainServer->prepareTestRequest(ui->btnGrpRequests->checkedId());
}


void Widget::on_btnClearSendBuffer_clicked()
{
    mainServer->clearTestArray();
}

void Widget::on_btnListDbDriver_clicked(){
    QStringList drivers;

    drivers = QSqlDatabase::drivers();
    for(int i = 0; i < drivers.size(); i++) {
        ui->edtLog->appendPlainText(drivers.at(i));
    }
}

void Widget::on_btnConnectDb_clicked()
{
    mainServer->connectDbServer();
}

void Widget::on_btnDbListBookings_clicked()
{
    mainServer->listBookings();
}


void Widget::on_pushButton_clicked()
{
    QString outString;
    QDateTime now = QDateTime::currentDateTimeUtc();
    outString = now.toString("yyyyMMddhhmmss");
    ui->edtLog->appendPlainText(outString);
}


void Widget::on_btnOpenDoor_clicked()
{
    mainServer->openDoor(2864434397);
}

void Widget::updateServerListeningStatus(bool status){
    if(true == status){
        ui->edtStatusServer->setStyleSheet("background-color: rgb(0, 255, 0);");
        ui->edtStatusServer->setText("isListening");
    }
    else{
        ui->edtStatusServer->setStyleSheet("background-color: rgb(170, 0, 0);");
        ui->edtStatusServer->setText("closed");
    }
}

void Widget::on_btnTestRequestCardToDb_clicked()
{
    mainServer->prepareTestRequest(-2);
    mainServer->sendRequestInternalTesting();
}

