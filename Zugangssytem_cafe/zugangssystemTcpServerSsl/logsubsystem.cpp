#include "logsubsystem.h"
#include "qdebug.h"

LogSubsystem::LogSubsystem(QObject *parent)
    : QObject(parent)
{

}

void LogSubsystem::logData(QString logString)
{
    qDebug() << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss") << logString;
}

void LogSubsystem::logServerListenStatus(bool logStatus)
{
    if(true == logStatus)
        qDebug() << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss") << "server is listening";
    else
        qDebug() << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss") << "Error: server is not listening";
}

