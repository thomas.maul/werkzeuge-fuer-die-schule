# Werkzeuge für die Schule

Hier entsteht in loser Folge eine Sammlung von Werkzeugen für den Schuunterricht.

Ich unterrrichte an einer beruflichen Schule Elektrotechnik und Informatik.

In Informatik arbeiten die Schüler:innen viel eigenständig am PC. Dabei gibt es Fragen.

Damit alle in der richtigen Reihenfolge bearbeitet werden gibt es die biologische-F1-Taste (Hilfe auf 2 Beinen, siehe Unterverzeichnis)

Das Programm funktioniert in unserer Schule. Es kann sein, dass Einzelen Teile zu spezifisch sind (z.B. Zugriff auf Anmeldenamen, ...)

Die Kommunikation ist noch nicht verschlüsselt.

## Contributing
Wer sich am projekt beteileigen möchte ist herzlich willommen. Meldet euch am besten per mail.

## Authors and acknowledgment
Erstellt (oder verbrochen) von Thomas Maul

## License
GPL V3 oder später 

## Project status
Das Projekt biologische-f1-Taste ist grundsätzlich getestet und war bereits im produktiven einsatz.

## Planung:
ich möchte die kommunikation auf SSL/ TLS umstellen. Dies kann jedoch dauern, da ich das Projekt mit wenig Prio betreibe.
