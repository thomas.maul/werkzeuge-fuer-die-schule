#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpSocket>
#include <QByteArray>
#include <vector>

#include "../biologische_f1_srv/request.h"
#include "../biologische_f1_srv/biological_f1_global_typs_defines.h"

using namespace std;

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    enum messageStage{messageStageNoMessage = -1, messageStageNewMessage, messageStageMessageRecPart, messageStageMessageRecComplete};
    const int tabRequestsMainListRows = 250;
//    enum sendMessagetype{NetCommWelcomeSend = 0x01, NetCommByeByeSend = 0x09,
//                         NetCommRequestNewSend = 0x11, NetCommRequestSolvedSend = 0x12, NetCommRequestCancelSend = 0x19,
//                         NetCommFaqNewSend = 0x20, NetCommRequestToFaqSend = 0x21, NetCommFaqNrSend = 0x22};
//    enum receivedMessagetype{NetCommNone = 0x00,
//                             NetCommHelloRec = 0x01, NetCommByeByeRec = 0x09,
//                             NetCommRequestNewRec = 0x10, NetCommRequestSolvedRec = 0x12, NetCommRequestCancelRec = 0x19,
//                             NetCommFaqNewRec = 0x20, NetCommRequestToFaqRec = 0x21,
//                             NetCommGetRequestList = 0x30, NetCommGetRequestListClear = 0x31, NetCommGetRequestListEntry = 0x32};
private slots:
    void on_btnConnectServer_clicked();
    void socketStateChanged(QAbstractSocket::SocketState socketState);
    void requestUpdate();

    void on_rbShowActiveRequests_toggled(bool checked);
    void readMessage();
    void computeMessage(QString message);
    void helperTableWidget(int &row, vector<Request*>::iterator it);
    void updateRequestTable();
    void on_btnReload_clicked();

private:
    Ui::Widget *ui;
    QTcpSocket *displaySocket;
    messageStage recMessageStage;
    QByteArray netInputBuffer;
    int bytesInMessage;
    vector<Request*> requestList;
};

#endif // WIDGET_H
