#-------------------------------------------------
#
# Project created by QtCreator 2017-09-02T13:12:36
#
#-------------------------------------------------

QT += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = biogical_f1_display
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    ../biologische_f1_srv/request.cpp

HEADERS  += widget.h \
    ../biologische_f1_srv/request.h \
    ../biologische_f1_srv/biological_f1_global_typs_defines.h

FORMS    += widget.ui
