#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget){
    ui->setupUi(this);

    displaySocket = new QTcpSocket;
    QObject::connect(displaySocket, &QTcpSocket::readyRead, this, &Widget::readMessage);
    QObject::connect(displaySocket, &QTcpSocket::stateChanged, this, &Widget::socketStateChanged);

    bytesInMessage = 0;
    recMessageStage = messageStageNoMessage;
}

Widget::~Widget(){
    delete ui;
}

void Widget::on_btnConnectServer_clicked(){
    displaySocket->connectToHost(ui->edtServerIP->text(), ui->edtServerPort->text().toInt());

    qDebug() << "connecting";
}

void Widget::socketStateChanged(QAbstractSocket::SocketState socketState){
    QString sendString;
    char bytes[2];

    if(QAbstractSocket::ConnectedState == socketState){
        sendString += "0x01";
        sendString += "§§";
        sendString += "0x03";

        unsigned short n = sendString.size();

        QByteArray sendBuffer;

        bytes[0] = (n >> 8) & 0xFF;
        bytes[1] = n & 0xFF;

        sendBuffer.append(bytes[0]);
        sendBuffer.append(bytes[1]);
        sendBuffer.append(sendString);

        displaySocket->write(sendBuffer);
        requestUpdate();

        ui->btnConnectServer->setEnabled(false);

        qDebug() << "connected, send: " << sendBuffer;
    }
    else{
        ui->btnConnectServer->setEnabled(true);
    }
}

void Widget::requestUpdate(){
    QString sendString;
    QString tempString;
    char bytes[2];

    sendString += "0x30";
    sendString += "§§";
    if(ui->rbShowActiveRequests->isChecked())
        sendString += "0x01";
    else
        sendString += "0x00";

    unsigned short n = sendString.size();

    QByteArray sendBuffer;

    bytes[0] = (n >> 8);
    bytes[1] = n;

    sendBuffer.append(bytes[0]);
    sendBuffer.append(bytes[1]);
    sendBuffer.append(sendString);

    displaySocket->write(sendBuffer);
}

void Widget::on_rbShowActiveRequests_toggled(bool checked)
{
    requestUpdate();
}

void Widget::readMessage(){
    do{
        if((messageStageNoMessage == recMessageStage)
                ||(messageStageMessageRecComplete == recMessageStage)){
            if(displaySocket->bytesAvailable() >= 2){
                netInputBuffer = displaySocket->read(2);// 2 Byte = Laenge vom Rest

                bytesInMessage = (netInputBuffer[0] >> 8) & 0xFF;
                bytesInMessage += netInputBuffer[1] & 0xFF;

                recMessageStage = messageStageNewMessage;
                netInputBuffer.clear();
            }
        }

        qDebug() << "bytesAvailable: " << displaySocket->bytesAvailable();

        if(messageStageNewMessage == recMessageStage){
            if(displaySocket->bytesAvailable() >= bytesInMessage){
                netInputBuffer += displaySocket->read(bytesInMessage);// alles auslesen
                recMessageStage = messageStageMessageRecComplete;

                qDebug() << "received data: " << netInputBuffer;
                computeMessage(netInputBuffer);
            }
            else{
                netInputBuffer = displaySocket->read(bytesInMessage);// alles auslesen, was geht
                recMessageStage = messageStageMessageRecPart;
            }
        }

        if(messageStageMessageRecPart == recMessageStage){
            if(displaySocket->bytesAvailable() >= bytesInMessage){
                netInputBuffer += displaySocket->read(bytesInMessage);// alles auslesen
                recMessageStage = messageStageMessageRecComplete;

                qDebug() << "received data: " << netInputBuffer;
                computeMessage(netInputBuffer);
            }
        }
    }
    while((displaySocket->bytesAvailable() >= 2)
          && ((messageStageNoMessage == recMessageStage)
              ||(messageStageMessageRecComplete == recMessageStage)));
}

void Widget::computeMessage(QString message){
    int recMessageType = 0xFF;
    QStringList messageList;
    QString partString;
    int requestNr = -1;
    int requestLen = 0;
    int partLen = 0;
    bool entryFound = false;
    bool ok = false;
    //    QString subject;
    //    QString text;
    //    Request::requestStateType state;
    //    QString name;
    //    QString realName;
    //    QString helperString;

    messageList = message.split("§§");
    recMessageType = messageList[0].toUShort(&ok, 16);

    //    recMessageType = computeRecMessage(netInputBuffer);

    Request *newRequestEntry = 0;
    Request *searchRequestEntry = 0;
    //    QString tempString;

    switch (recMessageType) {

    case ProtocolServerToCientBye: // disconnect me
        displaySocket->disconnectFromHost();
        displaySocket->deleteLater();
        break;

    case ProtocolServerToClientRequestListClear:
        for(vector<Request*>::iterator it2 = requestList.begin(); it2 != requestList.end(); ++it2){
            searchRequestEntry = *it2;
            delete searchRequestEntry;
            searchRequestEntry = 0;
        }
        requestList.clear();
        break;

    case ProtocolServerToClientRequestListEntry:
        requestLen = messageList[1].toInt();

        while(message.length()){

            partLen = message.indexOf("§§");
            message.remove(0, partLen + 2);
            partLen = message.indexOf("§§");
            message.remove(0, partLen + 2);

            partString =  message.left(requestLen);
            messageList = partString.split("§§");
            requestNr = messageList[0].toInt();

            for(vector<Request*>::iterator it2 = requestList.begin(); it2 != requestList.end(); ++it2){
                searchRequestEntry = *it2;
                if(searchRequestEntry->getRequestNr() == requestNr){
                    searchRequestEntry->setSubject(messageList[1]);
                    searchRequestEntry->setText(messageList[2]);
                    searchRequestEntry->setState(static_cast<Request::requestStateType>(messageList[3].toInt()));
                    searchRequestEntry->setName(messageList[4]);
                    searchRequestEntry->setRealName(messageList[5]);
                    searchRequestEntry->setHelperString(messageList[6]);
                    entryFound = true;

                    break;
                }
            }
            if(false == entryFound){
                newRequestEntry = new Request(messageList[0].toInt());
                newRequestEntry->setSubject(messageList[1]);
                newRequestEntry->setText(messageList[2]);
                newRequestEntry->setState(static_cast<Request::requestStateType>(messageList[3].toInt()));
                newRequestEntry->setName(messageList[4]);
                newRequestEntry->setRealName(messageList[5]);
                newRequestEntry->setHelperString(messageList[6]);
                requestList.push_back(newRequestEntry);
            }
            message.remove(0, requestLen + 2);
        }
        updateRequestTable();
        break;

    default:
        break;
    }
}

void Widget::updateRequestTable(){

    QTableWidgetItem *tempItem = 0;
    Request* tempRequest;
    Request::requestStateType state;
    bool showActiveRequests = ui->rbShowActiveRequests->isChecked();
    int row = 0;

    for(int i = 0; i < tabRequestsMainListRows; i++){
        tempItem = ui->tabRequestsMainList->takeItem(i, 0);
        delete tempItem;
        tempItem = ui->tabRequestsMainList->takeItem(i, 1);
        delete tempItem;
        tempItem = ui->tabRequestsMainList->takeItem(i, 2);
        delete tempItem;
        tempItem = ui->tabRequestsMainList->takeItem(i, 3);
        delete tempItem;
        tempItem = ui->tabRequestsMainList->takeItem(i, 4);
        delete tempItem;
    }


    ui->tabRequestsMainList->setSortingEnabled(false);
    for(vector<Request*>::iterator it = requestList.begin(); it != requestList.end(); ++it ){
        tempRequest = *it;
        state = tempRequest->getState();

        if(true == showActiveRequests){
            if((Request::requestRaised == state)||(Request::requestInProgress == state) ||(Request::requestReopend ==state)){
                helperTableWidget(row, it);
            }
        }
        else{
            helperTableWidget(row, it);
        }
    }
    ui->tabRequestsMainList->setSortingEnabled(true);
}

void Widget::helperTableWidget(int &row, vector<Request*>::iterator it){
    QTableWidgetItem *tempItemNr = 0;
    QTableWidgetItem *tempItemName = 0;
    QTableWidgetItem *tempItemSubject = 0;
    QTableWidgetItem *tempItemState = 0;
    QTableWidgetItem *tempItemHelper = 0;
    Request* tempRequest;
    QFont cellFont;
    QString stateText;
    Request::requestStateType state;

    tempRequest = *it;
    state = tempRequest->getState();

    tempItemNr = new QTableWidgetItem;
    tempItemName = new QTableWidgetItem;
    tempItemSubject = new QTableWidgetItem;
    tempItemState = new QTableWidgetItem;
    tempItemHelper = new QTableWidgetItem;

    switch(state){
    case Request::requestRaised:
        tempItemNr->setBackgroundColor(Qt::white);
        tempItemNr->setTextColor(Qt::red);
        cellFont = tempItemNr->font();
        cellFont.setBold(true);
        tempItemNr->setFont(cellFont);
        break;

    case Request::requestInProgress:
        tempItemNr->setBackgroundColor(Qt::yellow);
        tempItemNr->setTextColor(Qt::black);
        break;

    case Request::requestReopend:
        tempItemNr->setBackgroundColor(Qt::white);
        tempItemNr->setTextColor(Qt::black);
        break;

    default:
        tempItemNr->setBackgroundColor(Qt::white);
        tempItemNr->setTextColor(Qt::gray);
        break;
    }

    tempItemNr->setText(QString::number(tempRequest->getRequestNr()));
    tempItemName->setText(tempRequest->getName());
    tempItemSubject->setText(tempRequest->getSubject());

    switch (tempRequest->getState()) {
    case Request::requestRaised:
        stateText = "neue Anfrage";
        break;
    case Request::requestInProgress:
        stateText = "in Arbeit";
        break;
    case Request::requestSolved:
        stateText = "geholfen";
        break;
    case Request::requestClosed:
        stateText = "geschlossen";
        break;
    case Request::requestCanceld:
        stateText = "zurückgezogen";
        break;
    case Request::requestReopend:
        stateText = "ausgegraben";
        break;
    default:
        break;
    }
    tempItemState->setText(stateText);

    tempItemHelper->setText(tempRequest->getHelperString());

    ui->tabRequestsMainList->setItem(row, 0, tempItemNr);
    ui->tabRequestsMainList->setItem(row, 1, tempItemName);
    ui->tabRequestsMainList->setItem(row, 2, tempItemSubject);
    ui->tabRequestsMainList->setItem(row, 3, tempItemState);
    ui->tabRequestsMainList->setItem(row, 4, tempItemHelper);

    row++;
}

void Widget::on_btnReload_clicked(){
    requestUpdate();
}
