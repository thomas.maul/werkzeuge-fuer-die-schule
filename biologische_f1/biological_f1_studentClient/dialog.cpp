#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog){
    ui->setupUi(this);
}

Dialog::~Dialog(){
    delete ui;
}

QString Dialog::getName(){
    return ui->comboBox->currentText();
}

QString Dialog::getPassword(){
    return ui->lineEdit->text();
}
