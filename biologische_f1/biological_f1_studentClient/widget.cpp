#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget){
    ui->setupUi(this);
    clientSocket = new QTcpSocket;
    nextRequestNr = 0;

//    ui->tabWidget->setTabText(0, "Anfrage stellen / eigene Anfragen");
//    ui->tabWidget->setTabText(1, "Übersicht alle Anfragen");
}

Widget::~Widget(){
    delete ui;
}

void Widget::on_btnConnectServer_clicked(){
    clientSocket->connectToHost(ui->edtServerIP->text(), ui->edtServerPort->text().toInt());

    QObject::connect(clientSocket, &QTcpSocket::stateChanged, this, &Widget::socketStateChanged);
    QObject::connect(clientSocket, &QTcpSocket::readyRead, this, &Widget::readData);
}

void Widget::socketStateChanged(QAbstractSocket::SocketState socketState){
    QString sendString;
    QProcessEnvironment processEnvironment = QProcessEnvironment::systemEnvironment();
    char bytes[2];

    if(QAbstractSocket::ConnectedState == socketState){
        sendString += "0x01";
        sendString += "§§";
        sendString += "0x01";
        sendString += "§§";
        sendString += ui->edtUserName->text();
        sendString += "§§";
        sendString += processEnvironment.value("USER");
        sendString += processEnvironment.value("USERNAME");

        unsigned short n = sendString.size();

        QByteArray sendBuffer;

        bytes[0] = (n >> 8) & 0xFF;
        bytes[1] = n & 0xFF;

        sendBuffer.append(bytes[0]);
        sendBuffer.append(bytes[1]);
        sendBuffer.append(sendString);

        clientSocket->write(sendBuffer);
        ui->btnConnectServer->setEnabled(false);
        ui->btnSendRequest->setEnabled(true);
    }
    else{
        ui->btnConnectServer->setEnabled(true);
        ui->btnSendRequest->setEnabled(false);
    }
}


void Widget::readData(){
    if(messageStageNoMessage == recMessageStage){
        if(clientSocket->bytesAvailable() >= 2){
            netInputBuffer = clientSocket->read(2);// 2 Byte = Laenge vom Rest
            bytesInMessage = netInputBuffer.toShort();
            recMessageStage = messageStageNewMessage;
        }
    }
    if(messageStageNewMessage == recMessageStage){
        if(clientSocket->bytesAvailable() >= bytesInMessage){
            netInputBuffer += clientSocket->read(bytesInMessage);// alles auslesen
            recMessageStage = messageStageMessageRecComplete;

            //            computeMessage();
        }
        else{
            netInputBuffer = clientSocket->read(bytesInMessage);// alles auslesen, was geht
            recMessageStage = messageStageMessageRecPart;
        }
    }

    if(messageStageMessageRecPart == recMessageStage){
        if(clientSocket->bytesAvailable() >= bytesInMessage){
            netInputBuffer += clientSocket->read(bytesInMessage);// alles auslesen
            recMessageStage = messageStageMessageRecComplete;

            //            computeMessage();
        }
    }
}

void Widget::computeMessage(){

}

void Widget::on_edtUserName_editingFinished(){
    if(3 <= ui->edtUserName->text().length())
        ui->btnConnectServer->setEnabled(true);
    else
        ui->btnConnectServer->setEnabled(false);
}

void Widget::on_btnSendRequest_clicked(){
    QString sendString;
    QString tempString;
    char bytes[2];
    QByteArray sendBuffer;
    unsigned short n = 0;

    sendString += "0x10";
    sendString += "§§";

    sendString += QString::number(nextRequestNr);
    nextRequestNr++;
    sendString += "§§";

    tempString = ui->edtRequestSubject->text();
    tempString = stripText(tempString);
    sendString += tempString;
    sendString += "§§";

    tempString = ui->edtRequestText->toPlainText();
    tempString = stripText(tempString);
    sendString += tempString;
    sendString += "§§";
    sendString += ui->cbHelper->currentText();

    sendBuffer.append(sendString);

    n = sendBuffer.size();

    bytes[0] = (n >> 8) & 0xFF;
    bytes[1] = n & 0xFF;

    sendBuffer.insert(0,bytes[0]);
    sendBuffer.insert(1,bytes[1]);

    qDebug() << "send: " << sendBuffer << "len " << sendBuffer.length();

    clientSocket->write(sendBuffer);
}

//void Widget::on_btnRequestSolved_clicked()
//{
//    Dialog myDialog(this);

//    QString name;
//    QString password;

//    myDialog.setModal(true);
//    if(QDialog::Accepted == myDialog.exec())
//    {
//        name = myDialog.getName();
//        if(0 == userPin.compare(myDialog.getPassword()))
//        {
//            QString sendString;
//            char bytes[2];

//            sendString += "0x10";
//            sendString += "§§";
//            sendString += ui->edtRequestSubject->text();
//            sendString += "§§";
//            sendString += ui->edtRequestText->toPlainText();

//            unsigned short n = sendString.size();

//            QByteArray sendBuffer;

//            bytes[0] = (n >> 8);
//            bytes[1] = n;

//            sendBuffer.append(bytes[0]);
//            sendBuffer.append(bytes[1]);
//            sendBuffer.append(sendString);

//            clientSocket->write(sendBuffer);
//        }
//    }
//}

QString Widget::stripText(QString textIn){

    textIn.remove("§§");
    return textIn;
}
