#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpSocket>
#include <QProcessEnvironment>

#include "dialog.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    enum messageStage{messageStageNoMessage = -1, messageStageNewMessage, messageStageMessageRecPart, messageStageMessageRecComplete};

private slots:
    void on_btnConnectServer_clicked();
    void readData();

    void on_edtUserName_editingFinished();
//    void socketConnected();
    void socketStateChanged(QAbstractSocket::SocketState socketState);
    void on_btnSendRequest_clicked();
//    void on_btnRequestSolved_clicked();
    void computeMessage();
    QString stripText(QString textIn);

private:
    Ui::Widget *ui;

    QTcpSocket *clientSocket;
    messageStage recMessageStage;
    QByteArray netInputBuffer;
    unsigned short int bytesInMessage;
    QString userPin;
    int nextRequestNr;
};

#endif // WIDGET_H
