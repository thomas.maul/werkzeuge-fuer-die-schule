#-------------------------------------------------
#
# Project created by QtCreator 2017-08-26T13:06:27
#
#-------------------------------------------------

QT += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = biological_f1_studentClient
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    dialog.cpp

HEADERS  += widget.h \
    dialog.h

FORMS    += widget.ui \
    dialog.ui
