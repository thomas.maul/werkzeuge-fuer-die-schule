#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QString>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    QString getName();
    QString getPassword();

private:
    Ui::Dialog *ui;
//    QString name;
//    QString password;
};

#endif // DIALOG_H
