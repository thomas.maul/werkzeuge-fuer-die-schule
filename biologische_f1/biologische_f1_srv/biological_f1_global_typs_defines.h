#ifndef BIOLOGICAL_F1_GLOBAL_TYPS_DEFINES_H
#define BIOLOGICAL_F1_GLOBAL_TYPS_DEFINES_H

//Protokoll biologische F1-Taste
//			Gesamtaufbau
//			Länge (2 Byte) Befehl;<Parameter>
//Client an Server
//Befehl	Nr 	Parameter	Bedeutung
static const int ProtocolClientToServerHello = 0x01;//	Typ;AnzeigeName;Username;HelperRequested	Start mit Name
static const int ProtocolClientToServerBye = 0x09; //		Verbindung trennen

static const int ProtocolClientToServerRequestNew = 0x10;//	Betreff;Text 	neue Anfrage
static const int ProtocolClientToServerRequestSolved = 0x12; //	RequestNr	Anfrage wurde bearbeitet
static const int ProtocolClientToServerRequestInProgress = 0x13; //	RequestNr	Anfrage wird jetzt bearbeitet
static const int ProtocolClientToServerRequestPaused = 0x14; //	RequestNr	Bearbeitung ist unterbrochen, noch nicht abgeschlossen
static const int ProtocolClientToServerRequestCancel = 0x19; //	RequestNr	Anfrage zurückziehen

static const int ProtocolClientToServerFaqNew = 0x20;	//FaqNr;Betreff;Text 	Allgemeine Frage (Nr ist immer 0)
static const int ProtocolClientToServerRequestToFaq = 0x21;	//RequestNr	Einzelfrage zu allgemeiner

static const int ProtocolClientToServerGetRequestList	= 0x30;	//ListType[0=all,1=active]

static const int ProtocolClientToServerHeartbeatReply	= 0x81;	//HbNr	HbNr + 1


//Server an Client
//Befehl	Nr 	Parameter	Bedeutung
static const int ProtocolServerToClientWelcome = 0x01;	//String	Willkommen
static const int ProtocolServerToCientBye = 0x09; //		Verbindung trennen

static const int ProtocolServerToClientRequestNew = 0x11;	//RequestNr	Nummer für neue Anfrage vergeben
static const int ProtocolServerToClientRequestInProgress = 0x13;	//RequestNr	Anfrage wird jetzt bearbeitet
static const int ProtocolServerToClientRequestPaused = 0x14;	//RequestNr	Bearbeitung ist unterbrochen, noch nicht abgeschlossen
static const int ProtocolServerToClientRequestSolved = 0x12;	//RequestNr	Anfrage wurde bearbeitet
static const int ProtocolServerToClientRequestCancel = 0x19;	//RequestNr	Anfrage zurückziehen

static const int ProtocolServerToClientFaqNew	= 0x20;	//FaqNr;Betreff;Text 	Allgemeine Frage
static const int ProtocolServerToClientRequestToFaq = 0x21;	//RequestNr	Einzelfrage zu allgemeiner
static const int ProtocolServerToClientFaqNr = 0x22;	//FaqNr	erstellt Nr für Faq

static const int ProtocolServerToClientRequestListClear = 0x31;	//	Liste löschen
static const int ProtocolServerToClientRequestListEntry = 0x32;	//RequestNr; Subject; text; stateText; name; realName; helperRequestedString;	ListenEintrag (wir n mal wiederholt)

static const int ProtocolServerToClientHeartbeatRequest = 0x80;	//HbNr	Abfrage, ob Verbindung noch existiert

//ClientType
static const int ProtocolClientTypeStudent = 0x01;
static const int ProtocolClientTypeTeacher = 0x02;
static const int ProtocolClientTypeDisplay = 0x03;




#endif // BIOLOGICAL_F1_GLOBAL_TYPS_DEFINES_H
