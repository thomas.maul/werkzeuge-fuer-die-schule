#include "srvclientstuff.h"

SrvClientStuff::SrvClientStuff(QObject *parent) : QObject(parent){
    heartbeatCountDown = 10;
    clientSocket = 0;
    bytesInMessage = 0;
    messageValid = false;
    mySrvCommunicationNet = 0;
    myClientType = clientTypeNoClient;
    openRequests = 0;
    maxOpenRequests = 2;
    recMessageStage = messageStageNoMessage;
}

void SrvClientStuff::setMyClientStage(const clientStage &value)
{
    myClientStage = value;
}

SrvClientStuff::clientStage SrvClientStuff::getMyClientStage() const
{
    return myClientStage;
}

SrvClientStuff::SrvClientStuff(QTcpSocket *clientSocketIn, SrvClientCommunicationNet *mySrvCommunicationNetIn, QObject *parent)
    : SrvClientStuff(parent){
    clientSocket = clientSocketIn;
    mySrvCommunicationNet = mySrvCommunicationNetIn;
    QObject::connect(clientSocket, &QTcpSocket::readyRead, this, &SrvClientStuff::readMessage);
    myClientStage = clientStageConnectionEstablished;
}

//void SrvClientStuff::setClientSocket(QTcpSocket *clientSocketIn){
//    clientSocket = clientSocketIn;
//    QObject::connect(clientSocket, &QTcpSocket::readyRead, this, &SrvClientStuff::readMessage);
//}

void SrvClientStuff::setClientName(QString clientNameIn){
    clientName = clientNameIn;
}

void SrvClientStuff::setClientRealName(QString clientNameIn){
    clientRealName = clientNameIn;
}

void SrvClientStuff::setClientType(int clientTypeIn){
    switch(clientTypeIn){
    case 1:
        myClientType = clientTypeStudent;
        break;

    case 2:
        myClientType = clientTypeTeacher;
        break;

    case 3:
        myClientType = clientTypeDisplay;
        break;

    default:
        myClientType = clientTypeNoClient;
    }
}

bool SrvClientStuff::appendRequest(Request *tempRequest){
    Request *tempRequest2 = 0;

    for(vector<Request*>::iterator it = studentRequestVector.begin(); it != studentRequestVector.end(); ++it){
        tempRequest2 = *it;
        if(Request::requestRaised == tempRequest2->getState())
            return false;
    }

    tempRequest->setName(clientName);
    studentRequestVector.push_back(tempRequest);
    return true;
}

void SrvClientStuff::updateRequestState(Request *tempRequestIn){
    Request *tempRequest = 0;
    int requestNr = tempRequestIn->getRequestNr();

    for(vector<Request*>::iterator it = studentRequestVector.begin(); it != studentRequestVector.end(); ++it){
        tempRequest = *it;

        if(requestNr == tempRequest->getRequestNr()){
            tempRequest->setState(tempRequestIn->getState());
            break;
        }
    }
}

Request *SrvClientStuff::getRequest(int requestNr){
    Request *tempRequest = 0;

    for(vector<Request*>::iterator it = studentRequestVector.begin(); it != studentRequestVector.end(); ++it){
        tempRequest = *it;

        if(requestNr == tempRequest->getRequestNr())
            return tempRequest;
    }
    return 0;
}

SrvClientStuff::clientType SrvClientStuff::getClientType(){
    return myClientType;
}

//void SrvClientStuff::setMessageToSend(QString messageIn){
//    messageToSend = messageIn;
//}

vector<Request*> SrvClientStuff::getRequests() const{
    return studentRequestVector;
}

QString SrvClientStuff::getName(){
    return clientName;
}

QString SrvClientStuff::getRealName(){
    return clientRealName;
}


void SrvClientStuff::readMessage(){
    do{
        if(  (messageStageNoMessage == recMessageStage)
             ||(messageStageMessageRecComplete == recMessageStage)){
            if(clientSocket->bytesAvailable() >= 2){
                netInputBuffer = clientSocket->read(2);// 2 Byte = Laenge vom Rest

                bytesInMessage = (netInputBuffer[0] >> 8);// & 0xFF;
                bytesInMessage += netInputBuffer[1];// & 0xFF;

                recMessageStage = messageStageNewMessage;
                netInputBuffer.clear();
            }
        }

        qDebug() << "bytesAvailable: " << clientSocket->bytesAvailable();

        if(messageStageNewMessage == recMessageStage){
            if(clientSocket->bytesAvailable() >= bytesInMessage){
                netInputBuffer += clientSocket->read(bytesInMessage);// alles auslesen
                recMessageStage = messageStageMessageRecComplete;

                computeMessage();
            }
            else{
                netInputBuffer = clientSocket->read(bytesInMessage);// alles auslesen, was geht
                recMessageStage = messageStageMessageRecPart;
            }
        }

        if(messageStageMessageRecPart == recMessageStage){
            if(clientSocket->bytesAvailable() >= bytesInMessage){
                netInputBuffer += clientSocket->read(bytesInMessage);// alles auslesen
                recMessageStage = messageStageMessageRecComplete;

                computeMessage();
            }
        }
    }
    while((clientSocket->bytesAvailable() >= 2)
          && ((messageStageNoMessage == recMessageStage)
              ||(messageStageMessageRecComplete == recMessageStage)));
}

void SrvClientStuff::computeMessage(){
    QString message;
    SrvClientCommunicationNet::receivedMessagetype recMessageType;
    Request *tempRequest = 0;
    QString tempString;

    recMessageType = mySrvCommunicationNet->computeRecMessage(netInputBuffer, this);

    switch (recMessageType) {
    //all
    case SrvClientCommunicationNet::NetCommHelloRec: // got hello
        //        clientName = mySrvCommunicationNet->getClientName();
        //        myClientIsA = SrvClientStuff::clientTypeStudent;
        message = mySrvCommunicationNet->computeSendMessage(SrvClientCommunicationNet::NetCommWelcomeSend);
        sendMessage(message);

        emit signalClientMessage("new student: " + clientName);
        break;

        //all
    case SrvClientCommunicationNet::NetCommByeByeRec: // disconnect me
        message = mySrvCommunicationNet->computeSendMessage(SrvClientCommunicationNet::NetCommByeByeSend);
        sendMessage(message);

        emit signalClientMessage("connection closed: " + clientName);
        clientSocket->disconnectFromHost();
        clientSocket->deleteLater();
        break;

        //student
    case SrvClientCommunicationNet::NetCommRequestNewRec:
        if(0 < studentRequestVector.size()){
            tempRequest = studentRequestVector.back();
            tempRequest->setName(clientName);
            tempRequest->setRealName(clientRealName);
            tempString = clientName + " : " + QString::number(tempRequest->getRequestNr()) + tempRequest->getSubject();

            emit signalClientMessage(tempString);
        }
        emit requestStateChanged(tempRequest);
        break;

    case SrvClientCommunicationNet::NetCommRequestSolvedRec: // got RequestSolved

        emit requestStateChanged(tempRequest);
        break;

    case SrvClientCommunicationNet::NetCommRequestCancelRec: // got RequestCancel

        emit requestStateChanged(tempRequest);
        break;

    case SrvClientCommunicationNet::NetCommFaqNewRec: // got FaqNew

        emit faqStateChanged();
        break;

    case SrvClientCommunicationNet::NetCommRequestToFaqRec: // got RequestToFaq

        emit faqStateChanged();
        emit requestStateChanged(tempRequest);
        break;


    case SrvClientCommunicationNet::NetCommRequestUpdateRequestList:
        message = mySrvCommunicationNet->computeSendMessage(SrvClientCommunicationNet::NetCommDisplayListClear);
        sendMessage(message);
        message = mySrvCommunicationNet->computeSendMessage(SrvClientCommunicationNet::NetCommDisplayListAppend);
        sendMessage(message);
        break;

    default:
        break;

    }
}

void SrvClientStuff::sendUpdateRecord(Request *tempEntry){
    QString message;

    message = mySrvCommunicationNet->computeSendMessageDisplayRecord(tempEntry);
    sendMessage(message);
}

void SrvClientStuff::sendMessage(QString sendMessage){
    QByteArray sendBuffer;
    //    QDataStream outStream(&sendBuffer, QIODevice::WriteOnly);
    //    outStream.setVersion(QDataStream::Qt_5_7);

    char bytes[2];
    unsigned short n = sendMessage.size();

    sendBuffer.clear();

    bytes[0] = (n >> 8) & 0xFF;
    bytes[1] = n & 0xFF;
    sendBuffer.append(bytes[0]);
    sendBuffer.append(bytes[1]);
    sendBuffer.append(sendMessage);

    clientSocket->write(sendBuffer);
}

void SrvClientStuff::socketStateChanged(QAbstractSocket::SocketState socketState){

    switch (socketState) {
    case QAbstractSocket::ConnectedState:
        myClientStage = clientStageConnectionEstablished;
        break;

        //    case QAbstractSocket::ClosingState:
    case QAbstractSocket::UnconnectedState:
        clientSocket->close();
        clientSocket->deleteLater();
        myClientStage = clientStagePendingToDelete;
        break;
    default:
        break;
    }
}
