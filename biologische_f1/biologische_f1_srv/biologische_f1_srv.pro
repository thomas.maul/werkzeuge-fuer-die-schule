#-------------------------------------------------
#
# Project created by QtCreator 2017-08-24T19:34:01
#
#-------------------------------------------------

QT += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = biologische_f1_srv
TEMPLATE = app


SOURCES += main.cpp\
        srvmainwindow.cpp \
    srvmain.cpp \
    request.cpp \
    srvclientcommunicationnet.cpp \
    srvclientstuff.cpp \
    dialogrequestdetailsandadmin.cpp \
    dialogpin.cpp

HEADERS  += srvmainwindow.h \
    srvmain.h \
    request.h \
    srvclientcommunicationnet.h \
    srvclientstuff.h \
    dialogrequestdetailsandadmin.h \
    dialogpin.h \
    biological_f1_global_typs_defines.h

FORMS    += srvmainwindow.ui \
    dialogrequestdetailsandadmin.ui \
    dialogpin.ui
