#include "srvclientcommunicationnet.h"
#include "srvclientstuff.h"
#include "srvmain.h"

SrvClientCommunicationNet::SrvClientCommunicationNet(){
    nextRequestNr = 0;
}

SrvClientCommunicationNet::SrvClientCommunicationNet(SrvMain *parent) : QObject(parent){
    SrvClientCommunicationNet::parent = parent;
    nextRequestNr = 0;
}

SrvClientCommunicationNet::receivedMessagetype SrvClientCommunicationNet::computeRecMessage(QString message, SrvClientStuff* caller){
    QStringList messageList;
    QString clientName;
    QString clientRealName;
    int clientType;
    Request *tempRequest = 0;
    receivedMessagetype messageType = NetCommNone;
    //    Request::requestHelperType helperRequested = Request::helperUnknown;

    unsigned short int commandNr = 0x00;
    bool ok = false;

    messageList = message.split("§§");
    qDebug() << "rec: " << message;

    commandNr = messageList[0].toUShort(&ok, 16);

    switch (commandNr) {
    case ProtocolClientToServerHello: // got hello
        clientType = messageList[1].toInt(&ok, 16);

        if(messageList.size() > 2){
            clientName = messageList[2];
            clientRealName = messageList[3];
            caller->setClientName(clientName);
            caller->setClientRealName(clientRealName);
        }
        caller->setClientType(clientType);
        messageType = SrvClientCommunicationNet::NetCommHelloRec;
        //        computeSendMessage(SrvClientCommunicationNet::NetCommWelcomeSend);

        break;

    case ProtocolClientToServerBye: // got Bye
        messageType = SrvClientCommunicationNet::NetCommByeByeRec;
        //        computeSendMessage(SrvClientCommunicationNet::NetCommByeByeSend);

        break;

    case ProtocolClientToServerRequestNew: // got RequestNew
        tempRequest = new Request(nextRequestNr);
        tempRequest->setReqestClientNr(messageList[1].toInt());
        tempRequest->setSubject(messageList[2]);
        tempRequest->setText(messageList[3]);
        if(3 < messageList.count()){
            tempRequest->setHelperString(messageList[4]);
            //            helperRequested = findHelperRequested(messageList[3]);
            //            tempRequest->setHelperRequested(helperRequested);
        }
        tempRequest->setState(Request::requestRaised);
        if( true == caller->appendRequest(tempRequest))
            nextRequestNr++;

        messageType = SrvClientCommunicationNet::NetCommRequestNewRec;
        break;

    case ProtocolClientToServerRequestSolved: // got RequestSolved

        break;

    case ProtocolClientToServerRequestCancel: // got RequestCancel

        break;

    case ProtocolClientToServerFaqNew: // got FaqNew

        break;

    case ProtocolClientToServerRequestToFaq: // got RequestToFaq

        break;

    case ProtocolClientToServerGetRequestList:
        messageType = SrvClientCommunicationNet::NetCommRequestUpdateRequestList;
        break;

    default:
        break;
    }

    return messageType;
}

QString SrvClientCommunicationNet::computeSendMessage(sendMessagetype sendMessageType){
    QString sendString;
    QString tempString;
    vector<Request*> allClientRequestList;
    Request* tempEntry = 0;
    //    int sendStringSize = 0;
    //    QString msgToSend;

    switch (sendMessageType) {
    case NetCommWelcomeSend:
        sendString = QString::number(ProtocolClientToServerHello);
        break;

    case NetCommByeByeSend:
        sendString = QString::number(ProtocolServerToCientBye);
        break;

    case NetCommRequestNewSend:

        break;

    case NetCommRequestSolvedSend:

        break;

    case  NetCommRequestCancelSend:

        break;

    case NetCommFaqNewSend:

        break;

    case NetCommRequestToFaqSend:

        break;

    case NetCommFaqNrSend:

        break;

    case NetCommDisplayListClear:
        sendString = "0x31"; //QString::number(ProtocolServerToClientRequestListClear, 16).toUpper();
        break;

    case NetCommDisplayListAppend:
        parent->collectRequestList();
        allClientRequestList = parent->getRequestList();

        for(vector<Request*>::iterator it = allClientRequestList.begin(); it != allClientRequestList.end(); ++it){
            tempEntry = *it;

            tempString = QString::number(tempEntry->getRequestNr());
            tempString += "§§";
            tempString += tempEntry->getSubject();
            tempString += "§§";
            tempString += tempEntry->getText();
            tempString += "§§";
            tempString += QString::number(tempEntry->getState());
            tempString += "§§";
            tempString += tempEntry->getName();
            tempString += "§§";
            tempString += tempEntry->getRealName();
            tempString += "§§";
            tempString += tempEntry->getHelperString();
            tempString += "§§";

            sendString += "0x32";
            sendString += "§§";
            sendString += QString::number(tempString.length());
            sendString += "§§";
            sendString += tempString;
        }
        break;

    default:
        break;
    }

    qDebug() << "send: " << sendString;
    return sendString;
}

QString SrvClientCommunicationNet::computeSendMessageDisplayRecord(Request* tempEntry){
    QString tempString;
    QString sendString;

    tempString = QString::number(tempEntry->getRequestNr());
    tempString += "§§";
    tempString += tempEntry->getSubject();
    tempString += "§§";
    tempString += tempEntry->getText();
    tempString += "§§";
    tempString += QString::number(tempEntry->getState());
    tempString += "§§";
    tempString += tempEntry->getName();
    tempString += "§§";
    tempString += tempEntry->getRealName();
    tempString += "§§";
    tempString += tempEntry->getHelperString();
    tempString += "§§";

    sendString += "0x32";
    sendString += "§§";
    sendString += QString::number(tempString.length());
    sendString += "§§";
    sendString += tempString;

    qDebug() << "send: " << sendString;
    return sendString;
}

//bool SrvClientCommunicationNet::getSendMessage(QString &messageToSend)
//{
//    if(true == sendMessageValid)
//        messageToSend = SrvClientCommunicationNet::messageToSend;

//    return sendMessageValid;
//}


Request::requestHelperType SrvClientCommunicationNet::findHelperRequested(QString textIn){
    textIn = textIn.toLower();
    if(textIn.compare("egal/alle"))
        return Request::helperAll;
    if(textIn.compare("lehrkraft"))
        return Request::helperTeacher;
    if(textIn.compare("schüler"))
        return Request::helperStudent;
    if(textIn.compare("hr kunkel"))
        return Request::helperKunkel;
    if(textIn.compare("hr maul"))
        return Request::helperMaul;

    return Request::helperUnknown;
}
