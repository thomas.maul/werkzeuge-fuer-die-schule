#ifndef DIALOGREQUESTDETAILSANDADMIN_H
#define DIALOGREQUESTDETAILSANDADMIN_H

#include <QDialog>
#include <QString>
#include "request.h"

namespace Ui {
class DialogRequestDetailsAndAdmin;
}

class DialogRequestDetailsAndAdmin : public QDialog
{
    Q_OBJECT

public:
    explicit DialogRequestDetailsAndAdmin(QWidget *parent = 0);
    ~DialogRequestDetailsAndAdmin();

    void setSubject(QString subjectIn);
    void setRequestNr(int requestNrIn);
    void setText(QString textIn);
    void setState(Request::requestStateType stateIn);
    void setName(QString nameIn);
    Request::requestStateType getState();
    QString getPin();

private slots:

private:
    Ui::DialogRequestDetailsAndAdmin *ui;

    int requestNr;
    QString subject;
    QString text;
    Request::requestStateType state;
    QString name;

};

#endif // DIALOGREQUESTDETAILSANDADMIN_H
