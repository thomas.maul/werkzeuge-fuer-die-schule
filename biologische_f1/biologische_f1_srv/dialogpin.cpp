#include "dialogpin.h"
#include "ui_dialogpin.h"

DialogPin::DialogPin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogPin){
    ui->setupUi(this);
}

DialogPin::~DialogPin(){
    delete ui;
}


int DialogPin::getPin(){
    return ui->edtPin->text().toInt();
}
