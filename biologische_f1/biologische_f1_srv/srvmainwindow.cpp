#include "srvmainwindow.h"
#include "ui_srvmainwindow.h"

SrvMainWindow::SrvMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SrvMainWindow){
    ui->setupUi(this);

    ui->tabRequestsMainList->setRowCount(tabRequestsMainListRows);
    ui->tabRequestsMainList->setColumnCount(5);

    currentRow = 0;

    mySrvMain = new SrvMain(this);
    QObject::connect(mySrvMain, &SrvMain::signalSrvMessage, this, &SrvMainWindow::showLogMessage);
    QObject::connect(mySrvMain, &SrvMain::signalNewIpAddress, this, &SrvMainWindow::showNewIpAddress);
    QObject::connect(mySrvMain, &SrvMain::updateRequestDisplay, this, &SrvMainWindow::updateRequestTable);

    QObject::connect(this, &SrvMainWindow::signalStartServer, mySrvMain, &SrvMain::startServer);
    QObject::connect(this, &SrvMainWindow::signalStopServer, mySrvMain, &SrvMain::stopServer);
    QObject::connect(ui->tabRequestsMainList, &QTableWidget::cellClicked, this, &SrvMainWindow::getClickedCell);
    QObject::connect(ui->tabRequestsMainList, &QTableWidget::cellDoubleClicked, this, &SrvMainWindow::getDoubleClickedCell);

    DialogPin myDialogPin;

    if(true == myDialogPin.exec()){
        teacherPin = myDialogPin.getPin();
    }
}

SrvMainWindow::~SrvMainWindow(){
    delete ui;
}

void SrvMainWindow::on_btnSrvStartStop_clicked(){
    if(0 == ui->btnSrvStartStop->text().compare("starten")){
        QHostAddress srvAdress;
        if(ui->rbAllIp)
            srvAdress = QHostAddress::Any;
        else
            srvAdress = ui->edtIp->text();
        int socket = ui->edtPort->text().toInt();

        emit signalStartServer(srvAdress, socket);

        ui->btnSrvStartStop->setText("stoppen");
        ui->btnSrvStartStop_2->setText("stoppen");
    }
    else{
        emit signalStopServer();
        ui->btnSrvStartStop->setText("starten");
        ui->btnSrvStartStop_2->setText("starten");
    }
}

void SrvMainWindow::showLogMessage(QString message){
    ui->tedtLog->appendPlainText(message);
}

void SrvMainWindow::showNewIpAddress(QHostAddress newIpAddress){
    ui->laHostAdress->setText("ServerIP: " + newIpAddress.toString());
}

void SrvMainWindow::on_btnSrvStartStop_2_clicked(){
    on_btnSrvStartStop_clicked();
}

void SrvMainWindow::requestListChanged(){
    mySrvMain->collectRequestList();
    allClientRequestList = mySrvMain->getRequestList();
}

void SrvMainWindow::updateRequestTable(){

    QTableWidgetItem *tempItem = 0;
    Request* tempRequest;
    Request::requestStateType state;
    bool showActiveRequests = ui->rbShowActiveRequests->isChecked();
    int row = 0;

    for(int i = 0; i < tabRequestsMainListRows; i++){
        tempItem = ui->tabRequestsMainList->takeItem(i, 0);
        delete tempItem;
        tempItem = ui->tabRequestsMainList->takeItem(i, 1);
        delete tempItem;
        tempItem = ui->tabRequestsMainList->takeItem(i, 2);
        delete tempItem;
        tempItem = ui->tabRequestsMainList->takeItem(i, 3);
        delete tempItem;
        tempItem = ui->tabRequestsMainList->takeItem(i, 4);
        delete tempItem;
    }

    mySrvMain->collectRequestList();
    allClientRequestList = mySrvMain->getRequestList();

    ui->tabRequestsMainList->setSortingEnabled(false);
    for(vector<Request*>::iterator it = allClientRequestList.begin(); it != allClientRequestList.end(); ++it ){
        tempRequest = *it;
        state = tempRequest->getState();

        if(true == showActiveRequests){
            if((Request::requestRaised == state)||(Request::requestInProgress == state) ||(Request::requestReopend ==state)){
                helperTableWidget(row, it);
            }
        }
        else{
            helperTableWidget(row, it);
        }
    }
    //    ui->tabRequestsMainList->setSortingEnabled(true);
}

void SrvMainWindow::helperTableWidget(int &row, vector<Request*>::iterator it){
    QTableWidgetItem *tempItemNr = 0;
    QTableWidgetItem *tempItemName = 0;
    QTableWidgetItem *tempItemSubject = 0;
    QTableWidgetItem *tempItemState = 0;
    QTableWidgetItem *tempItemHelper = 0;
    Request* tempRequest;
    QFont cellFont;
    QString stateText;
    Request::requestStateType state;

    tempRequest = *it;
    state = tempRequest->getState();

    tempItemNr = new QTableWidgetItem;
    tempItemName = new QTableWidgetItem;
    tempItemSubject = new QTableWidgetItem;
    tempItemState = new QTableWidgetItem;
    tempItemHelper = new QTableWidgetItem;

    switch(state){
    case Request::requestRaised:
        tempItemNr->setBackgroundColor(Qt::white);
        tempItemNr->setTextColor(Qt::red);
        cellFont = tempItemNr->font();
        cellFont.setBold(true);
        tempItemNr->setFont(cellFont);
        break;

    case Request::requestInProgress:
        tempItemNr->setBackgroundColor(Qt::yellow);
        tempItemNr->setTextColor(Qt::black);
        break;

    case Request::requestReopend:
        tempItemNr->setBackgroundColor(Qt::white);
        tempItemNr->setTextColor(Qt::black);
        break;

    default:
        tempItemNr->setBackgroundColor(Qt::white);
        tempItemNr->setTextColor(Qt::gray);
        break;
    }

    tempItemNr->setText(QString::number(tempRequest->getRequestNr()));
    tempItemName->setText(tempRequest->getName());
    tempItemSubject->setText(tempRequest->getSubject());

    switch (tempRequest->getState()) {
    case Request::requestRaised:
        stateText = "neue Anfrage";
        break;
    case Request::requestInProgress:
        stateText = "in Arbeit";
        break;
    case Request::requestSolved:
        stateText = "geholfen";
        break;
    case Request::requestClosed:
        stateText = "geschlossen";
        break;
    case Request::requestCanceld:
        stateText = "zurückgezogen";
        break;
    case Request::requestReopend:
        stateText = "ausgegraben";
        break;
    default:
        break;
    }
    tempItemState->setText(stateText);

    tempItemHelper->setText(tempRequest->getHelperString());

    ui->tabRequestsMainList->setItem(row, 0, tempItemNr);
    ui->tabRequestsMainList->setItem(row, 1, tempItemName);
    ui->tabRequestsMainList->setItem(row, 2, tempItemSubject);
    ui->tabRequestsMainList->setItem(row, 3, tempItemState);
    ui->tabRequestsMainList->setItem(row, 4, tempItemHelper);

    row++;
}

void SrvMainWindow::on_btnReload_clicked(){
    updateRequestTable();
}

void SrvMainWindow::on_btnCancel_clicked(){
    QTableWidgetItem *tempItemNr = 0;
    int requestNr = -1;
    Request *tempRequest;

    tempItemNr = ui->tabRequestsMainList->item(currentRow, 0);
    if(0 == tempItemNr)
        return;

    requestNr = tempItemNr->text().toInt();

    mySrvMain->collectRequestList();
    allClientRequestList = mySrvMain->getRequestList();

    for(vector<Request*>::iterator it = allClientRequestList.begin(); it != allClientRequestList.end(); ++it ){
        tempRequest = *it;
        if(requestNr == tempRequest->getRequestNr()){
            tempRequest->setState(Request::requestCanceld);
        }
    }

    updateRequestTable();
}

void SrvMainWindow::getClickedCell(int row, int column){
    currentRow = row;
}

void SrvMainWindow::on_btnSolved_clicked(){
    QTableWidgetItem *tempItemNr = 0;
    int requestNr = -1;
    Request *tempRequest;
    DialogPin myDialogPin;

    if(QDialog::Accepted == myDialogPin.exec()){

        if(myDialogPin.getPin() == teacherPin){

            tempItemNr = ui->tabRequestsMainList->item(currentRow, 0);
            if(0 == tempItemNr)
                return;

            requestNr = tempItemNr->text().toInt();

            mySrvMain->collectRequestList();
            allClientRequestList = mySrvMain->getRequestList();

            for(vector<Request*>::iterator it = allClientRequestList.begin(); it != allClientRequestList.end(); ++it ){
                tempRequest = *it;
                if(requestNr == tempRequest->getRequestNr()){
                    tempRequest->setState(Request::requestSolved);
                }
            }

            updateRequestTable();
        }
    }
}

void SrvMainWindow::getDoubleClickedCell(int row, int column){
    QTableWidgetItem *tempItemNr = 0;
    int requestNr = -1;
    Request *tempRequest;
    Request::requestStateType tempState = Request::requestNone;
    DialogRequestDetailsAndAdmin myDialog;

    tempItemNr = ui->tabRequestsMainList->item(row, 0);
    if(0 ==tempItemNr)
        return; //leeres Feld

    requestNr = tempItemNr->text().toInt();

    mySrvMain->collectRequestList();
    allClientRequestList = mySrvMain->getRequestList();

    for(vector<Request*>::iterator it = allClientRequestList.begin(); it != allClientRequestList.end(); ++it ){
        tempRequest = *it;
        if(requestNr == tempRequest->getRequestNr()){
            myDialog.setName(tempRequest->getName());
            myDialog.setSubject(tempRequest->getSubject());
            myDialog.setRequestNr(tempRequest->getRequestNr());
            myDialog.setText(tempRequest->getText());
            myDialog.setState(tempRequest->getState());
            statusBar()->showMessage(tempRequest->getRealName(), 2000);

            if(QDialog::Accepted == myDialog.exec()){
                if(myDialog.getPin().toInt() == teacherPin)
                    tempState = myDialog.getState();

                if(tempRequest->getState() != tempState){
                    tempRequest->setState(tempState);

                    updateRequestTable();
                    mySrvMain->requestStateChanged(tempRequest);

                    break;
                }
            }
        }
    }
}
