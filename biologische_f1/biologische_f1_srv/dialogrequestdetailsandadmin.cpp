#include "dialogrequestdetailsandadmin.h"
#include "ui_dialogrequestdetailsandadmin.h"

DialogRequestDetailsAndAdmin::DialogRequestDetailsAndAdmin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogRequestDetailsAndAdmin){
    ui->setupUi(this);
}

DialogRequestDetailsAndAdmin::~DialogRequestDetailsAndAdmin(){
    delete ui;
}

void DialogRequestDetailsAndAdmin::setSubject(QString subjectIn){
    subject = subjectIn;
    ui->edtSubject->setText(subjectIn);
}

void DialogRequestDetailsAndAdmin::setRequestNr(int requestNrIn){
    requestNr = requestNrIn;
    ui->edtRequestNr->setText(QString::number(requestNrIn));
}

void DialogRequestDetailsAndAdmin::setText(QString textIn){
    text = textIn;
    ui->edtText->appendPlainText(textIn);
}

void DialogRequestDetailsAndAdmin::setState(Request::requestStateType stateIn){
    state = stateIn;
    ui->cbState->setCurrentIndex(stateIn);
}

void DialogRequestDetailsAndAdmin::setName(QString nameIn){
    name = nameIn;
    ui->edtName->setText(nameIn);
}

Request::requestStateType DialogRequestDetailsAndAdmin::getState(){
    return static_cast<Request::requestStateType> (ui->cbState->currentIndex());
}

QString DialogRequestDetailsAndAdmin::getPin(){
    return ui->edtPin->text();
}
