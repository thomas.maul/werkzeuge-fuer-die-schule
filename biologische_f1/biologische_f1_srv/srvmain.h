#ifndef SRVMAIN_H
#define SRVMAIN_H

#include <QTcpServer>
#include <QMainWindow>
#include <QHostAddress>
#include <QMessageBox>
#include <QList>
#include <QObject>
#include <vector>
#include <QTimer>

#include "srvclientstuff.h"
#include "srvclientcommunicationnet.h"
#include "request.h"

using namespace std;

class SrvMainWindow;

class SrvMain : public QObject
{
    Q_OBJECT

public:
    explicit SrvMain(SrvMainWindow *parent = 0);

    void setHostAddress(QHostAddress srvAdressIn);
    void setSocket(int socketIn);
    vector<Request *> getRequestList() const;

public slots:
    void startServer(QHostAddress srvAdressIn, int socketIn);
    void stopServer();

    void collectRequestList();
    void requestStateChanged(Request *tempRequest);

private slots:
    void newClient();
    void mainTimerScheduler();

signals:
    void signalSrvMessage(QString message);
    void signalNewIpAddress(QHostAddress newIpAddress);
    void updateRequestDisplay();

private:
//    explicit SrvMain();

    QTcpServer *mainServer;
    int socket;
    QHostAddress srvAdress;
    list<SrvClientStuff*> clientList;
    vector<Request*> allClientRequestVector;
    SrvClientCommunicationNet *mySrvCommunicationNet;

    SrvMainWindow *widget;
    QTimer mainTimer;

};

#endif // SRVMAIN_H
