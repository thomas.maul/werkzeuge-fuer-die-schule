#ifndef SRVMAINWINDOW_H
#define SRVMAINWINDOW_H

#include <QMainWindow>
#include <QHostAddress>
#include <QObject>

#include "srvmain.h"
#include "srvclientcommunicationnet.h"
#include "dialogrequestdetailsandadmin.h"
#include "dialogpin.h"

class SrvMain;

namespace Ui {
class SrvMainWindow;
}

class SrvMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SrvMainWindow(QWidget *parent = 0);
    ~SrvMainWindow();


    const QColor colorBackgroundRequestInProgress = Qt::yellow;
    const QColor colorTextRequestClosed = Qt::lightGray;

    const int tabRequestsMainListRows = 250;

public slots:
    void showLogMessage(QString message);
    void showNewIpAddress(QHostAddress newIpAddress);
    void requestListChanged();
    void updateRequestTable();

signals:
    void signalStartServer(QHostAddress, int);
    void signalStopServer();

private slots:
    void on_btnSrvStartStop_clicked();

    void on_btnSrvStartStop_2_clicked();

    void on_btnReload_clicked();

    void on_btnCancel_clicked();

    void on_btnSolved_clicked();

    void getClickedCell(int row, int column);
    void getDoubleClickedCell(int row, int column);
    void helperTableWidget(int &row, vector<Request*>::iterator it);

private:
    Ui::SrvMainWindow *ui;
    QObject *parent;
    SrvMain *mySrvMain;

    SrvClientCommunicationNet *mySrvCommunicationNet;
    vector<Request*> allClientRequestList;

    int currentRow;
    int teacherPin;
};

#endif // SRVMAINWINDOW_H
