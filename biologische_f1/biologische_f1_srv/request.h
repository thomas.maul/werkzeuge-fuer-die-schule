#ifndef REQUEST_H
#define REQUEST_H

#include <QString>

class Request
{
public:
    Request(int nextNr);
    enum requestStateType{requestNone = -1, requestRaised, requestInProgress, requestSolved,
                          requestClosed, requestCanceld, requestReopend};
    enum requestListType{requestListAll, requestListOpen, requestListClosed};
    enum requestHelperType{helperUnknown = -1, helperAll, helperTeacher, helperStudent, helperKunkel, helperMaul};


    void setSubject(QString subjectIn);
    void setText(QString textIn);
    void setState(requestStateType stateIn);
    void setName(QString nameIn);
    void setRealName(QString realNameIn);
    void setRequested(QString nameIn);
    void setHelperRequested(requestHelperType helperRequestedIn);
    void setHelperString(QString helperRequestedIn);

    int getRequestNr();

    QString getSubject();
    QString getText();
    requestStateType getState();
    QString getName();
    QString getRealName();
    requestHelperType getHelperRequested();
    QString getHelperString();

    int getReqestClientNr() const;
    void setReqestClientNr(const int &value);

private:
    Request();

    int requestSrvNr;
    int reqestClientNr;
    QString subject;
    QString text;
    requestStateType state;
    QString name;
    QString realName;
    requestHelperType helperRequested;
    QString helperString;
};

#endif // REQUEST_H
