#ifndef DIALOGPIN_H
#define DIALOGPIN_H

#include <QDialog>

namespace Ui {
class DialogPin;
}

class DialogPin : public QDialog
{
    Q_OBJECT

public:
    explicit DialogPin(QWidget *parent = 0);
    ~DialogPin();

    int getPin();

private:
    Ui::DialogPin *ui;
};

#endif // DIALOGPIN_H
