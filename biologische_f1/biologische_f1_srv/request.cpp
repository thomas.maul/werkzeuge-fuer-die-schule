#include "request.h"

Request::Request(int nextNr){
    requestSrvNr = nextNr;
}

Request::Request(){

}

int Request::getReqestClientNr() const
{
    return reqestClientNr;
}

void Request::setReqestClientNr(const int &value)
{
    reqestClientNr = value;
}

void Request::setSubject(QString subjectIn){
    subject = subjectIn;
}

void Request::setText(QString textIn){
    text = textIn;
}

void Request::setState(requestStateType stateIn){
    state = stateIn;
}

void Request::setHelperRequested(Request::requestHelperType helperRequestedIn){
    helperRequested = helperRequestedIn;
}

void Request::setHelperString(QString helperRequestedIn){
    helperString = helperRequestedIn;
}


QString Request::getHelperString(){
    return helperString;
}

int Request::getRequestNr(){
    return requestSrvNr;
}

QString Request::getSubject()
{
    return subject;
}

QString Request::getText(){
    return text;
}

Request::requestStateType Request::getState(){
    return state;
}

void Request::setName(QString nameIn){
    name = nameIn;
}

void Request::setRealName(QString realNameIn){
    realName = realNameIn;
}

QString Request::getName(){
    return name;
}

QString Request::getRealName(){
    return realName;
}

Request::requestHelperType Request::getHelperRequested(){
    return helperRequested;
}
