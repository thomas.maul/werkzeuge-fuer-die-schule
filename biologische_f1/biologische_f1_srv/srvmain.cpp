#include "srvmain.h"
#include "srvmainwindow.h"

SrvMain::SrvMain(SrvMainWindow *parent) : QObject(parent){
    widget = parent;
    mainServer = new QTcpServer(this);
    mySrvCommunicationNet = new SrvClientCommunicationNet(this);
    connect(&mainTimer, &QTimer::timeout, this, &SrvMain::mainTimerScheduler);
    mainTimer.start(1000);
}

void SrvMain::setHostAddress(QHostAddress srvAdressIn){
    srvAdress = srvAdressIn;
}

void SrvMain::setSocket(int socketIn){
    socket = socketIn;
}

void SrvMain::startServer(QHostAddress srvAdressIn, int socketIn){
    socket = socketIn;
    srvAdress = srvAdressIn;
    if(false == mainServer->listen(srvAdress, socket)){
        QMessageBox::critical(widget, tr("Biological F1-Key"),
                              tr("Unable to start the server: %1.")
                              .arg(mainServer->errorString()));
    }
    else{
        QObject::connect(mainServer, &QTcpServer::newConnection, this, &SrvMain::newClient);

        emit signalSrvMessage("net Server start");

        emit signalNewIpAddress(mainServer->serverAddress());
    }
}

void SrvMain::stopServer(){
    mainServer->close();
    QObject::disconnect(mainServer, &QTcpServer::newConnection, this, &SrvMain::newClient);

    emit signalSrvMessage("net Server stop");
    emit signalNewIpAddress(QHostAddress::Null);
}

void SrvMain::newClient()
{
    QTcpSocket * tempSocket = 0;
    SrvClientStuff *tempEntry = 0;

    while(true == mainServer->hasPendingConnections()){
        tempSocket = mainServer->nextPendingConnection();
        tempEntry = new SrvClientStuff(tempSocket, mySrvCommunicationNet, this);
        clientList.push_back(tempEntry);

        QObject::connect(tempEntry, &SrvClientStuff::signalClientMessage, widget, &SrvMainWindow::showLogMessage);
        QObject::connect(tempEntry, &SrvClientStuff::requestStateChanged, this, &SrvMain::requestStateChanged);
    }
}

void SrvMain::mainTimerScheduler()
{
    SrvClientStuff *tempClient = 0;


    for (list<SrvClientStuff*>::iterator it = clientList.begin() ; it != clientList.end(); ++it){
        tempClient = *it;
        switch(tempClient->getMyClientStage())
        {
        case SrvClientStuff::clientStagependingToClose:
            tempClient->setMyClientStage(SrvClientStuff::clientStagePendingToDelete);
            break;

        case SrvClientStuff::clientStagePendingToDelete:
            delete tempClient;
            it = clientList.erase(it);

        default:
            ;
        }
    }
}

void SrvMain::collectRequestList(){
    SrvClientStuff *tempClient = 0;

    allClientRequestVector.clear();

    for (list<SrvClientStuff*>::iterator it = clientList.begin() ; it != clientList.end(); ++it){
        vector<Request*> localList;
        tempClient = *it;

        localList = tempClient->getRequests();
        for(vector<Request*>::iterator it2 = localList.begin(); it2 != localList.end(); ++it2){
            allClientRequestVector.push_back(*it2);
        }
    }
}

vector<Request*> SrvMain::getRequestList() const{
    return allClientRequestVector;
}

void SrvMain::requestStateChanged(Request* tempRequest){
    SrvClientStuff* tempEntryClient;

    emit updateRequestDisplay();

    for (list<SrvClientStuff*>::iterator it = clientList.begin() ; it != clientList.end(); ++it){
        tempEntryClient = *it;
        SrvClientStuff::clientType tempClientType = tempEntryClient->getClientType();
        switch(tempClientType)
        {
        case SrvClientStuff::clientTypeDisplay:
            tempEntryClient->sendUpdateRecord(tempRequest);
            break;
        case SrvClientStuff::clientTypeStudent:
            tempEntryClient->sendUpdateRecord(tempRequest);
            break;
        default:
            ;
        }
    }
}
