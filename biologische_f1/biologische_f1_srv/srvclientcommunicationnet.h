#ifndef SRVCOMMUNICATIONNET_H
#define SRVCOMMUNICATIONNET_H

#include <QObject>
#include "request.h"
#include "biological_f1_global_typs_defines.h"

class SrvClientStuff;
class SrvMain;

class SrvClientCommunicationNet : public QObject
{
    Q_OBJECT
public:
    explicit SrvClientCommunicationNet(SrvMain *parent);

    enum sendMessagetype{NetCommWelcomeSend = 0x01, NetCommByeByeSend = 0x09,
                         NetCommRequestNewSend = 0x11, NetCommRequestSolvedSend = 0x12, NetCommRequestCancelSend = 0x19,
                         NetCommFaqNewSend = 0x20, NetCommRequestToFaqSend = 0x21, NetCommFaqNrSend = 0x22,
                         NetCommDisplayListClear = 0x30, NetCommDisplayListAppend = 0x31, NetCommDisplayUpdateRecord = 0x32
                        };
    enum receivedMessagetype{NetCommNone = 0x00,
                             NetCommHelloRec = 0x01, NetCommByeByeRec = 0x09,
                             NetCommRequestNewRec = 0x10, NetCommRequestSolvedRec = 0x12, NetCommRequestCancelRec = 0x19,
                             NetCommFaqNewRec = 0x20, NetCommRequestToFaqRec = 0x21,
                             NetCommRequestUpdateRequestList = 0x30};
    enum clientType{noClient = -1, clientStudent, clientTeacher, clientDisplay};

    receivedMessagetype computeRecMessage(QString message, SrvClientStuff *caller);
    QString computeSendMessage(sendMessagetype sendMessageType);
    QString computeSendMessageDisplayRecord(Request* tempEntry);

//    bool getSendMessage(QString &messageToSend);

signals:

public slots:

private:
    explicit SrvClientCommunicationNet();
    unsigned int nextRequestNr;
    Request::requestHelperType findHelperRequested(QString textIn);
    SrvMain *parent;
};

#endif // SRVCOMMUNICATIONNET_H
