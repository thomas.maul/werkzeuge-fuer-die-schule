#ifndef SRVCLIENTINFO_H
#define SRVCLIENTINFO_H

#include <QString>
#include <QTcpSocket>
#include <QByteArray>
#include <QDataStream>
#include <QObject>
#include <QStringList>
#include <QDebug>
#include <vector>
#include "request.h"
#include "srvclientcommunicationnet.h"

using namespace std;

class SrvClientStuff : public QObject
{
    Q_OBJECT

public:
    SrvClientStuff(QTcpSocket *clientSocketIn, SrvClientCommunicationNet *mySrvCommunicationNetIn, QObject *parent);

    enum clientType {clientTypeNoClient = -1, clientTypeStudent, clientTypeTeacher, clientTypeDisplay};
    enum messageStage{messageStageNoMessage = -1, messageStageNewMessage, messageStageMessageRecPart,
                      messageStageMessageRecComplete};
    enum clientStage {clientStageNoClient = -1, clientStageNewConnect, clientStageConnectionEstablished,
                      clientStagependingToClose, clientStageClosed,clientStagePendingToDelete,
                      clientStageDeleted};

    //    void setClientSocket(QTcpSocket *clientSocketIn);
    void setClientName(QString clientNameIn);
    void setClientRealName(QString clientNameIn);
    void setClientType(int clientTypeIn);
    bool appendRequest(Request *tempRequest);
    void updateRequestState(Request *tempRequestIn);
    //    void setRecMessageType(SrvClientCommunicationNet::receivedMessagetype recMessageTypeIn);
    //    void setMessageToSend(QString messageIn);

    Request *getRequest(int requestNr);
    vector<Request*> getRequests() const;//Request::requestListType listType);
    QString getName();
    QString getRealName();
    clientType getClientType();
    void sendUpdateRecord(Request *tempEntry);

    clientStage getMyClientStage() const;

    void setMyClientStage(const clientStage &value);

signals:
    void signalClientMessage(QString message);
    void requestStateChanged(Request* tempRequest);
    void faqStateChanged();

private slots:
    void readMessage();
    void socketStateChanged(QAbstractSocket::SocketState socketState);

private:
    SrvClientStuff(QObject *parent = 0);

    QString clientName;
    QString clientRealName;
    clientType myClientType;
    QTcpSocket *clientSocket;
    int heartbeatCountDown;

    vector<Request*> studentRequestVector;
    int openRequests;

    QByteArray netInputBuffer;
    bool messageValid;
    unsigned short int bytesInMessage;
    SrvClientCommunicationNet *mySrvCommunicationNet;
    messageStage recMessageStage;
    int maxOpenRequests;
    clientStage myClientStage;

    void computeMessage();
    void sendMessage(QString sendMessage);
};

#endif // SRVCLIENTINFO_H
